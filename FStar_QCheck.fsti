module FStar_QCheck

open FStar.All

val arbitrary (a:Type) : Type

val assume_ : b:bool -> ML (u:unit{b})

// QCheck.Test module
val test_t : Type

val test_make : string -> arbitrary 'a -> ('a -> ML bool) -> test_t

// Caution: only for undefined generators!
// Move this later
val undefined_gen : a:Type -> string -> arbitrary a
