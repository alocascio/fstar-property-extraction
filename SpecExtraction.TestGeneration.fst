module SpecExtraction.TestGeneration

open FStar.Tactics
open FStar.InteractiveHelpers

open SpecExtraction.Util
open SpecExtraction.PrePost

// Turn a list of binders [x1:T1; x2:T2 ... ] into a nested dependent pair of the
// shape [x1:T1 & [x2:T2 & ...]]
// This type is used to pack the functions arguments, i.e. is the type of the
// generator.
val binder_list_to_dtuple :
  bs : list binder{List.Tot.length bs > 0} ->
  Tac typ
let rec binder_list_to_dtuple bs =
  match bs with
  | [b] -> get_bv_typ (get_binder_bv b)
  | b::bs ->
    let t1 = get_bv_typ (get_binder_bv b) in
    let bd2 = binder_list_to_dtuple bs in
    let t2 = pack (Tv_Abs b bd2) in
    `(dtuple2 (`#t1) (`#t2))

// Make the pattern (|x1, (| x2, ...|)|) for unpacking the generated arguments.
val make_pattern :
  bs:list binder{List.Tot.Base.length bs > 0} ->
  Tac (pattern * list bv)
let rec make_pattern bs =
  match bs with
  | [b] -> let bv = get_binder_bv b in (Pat_Var bv, [bv])
  | b::rest ->
    let (p2, bvs) = make_pattern rest in
    let bv = get_binder_bv b in
    let p1 = Pat_Var bv in
    let mkt2_fv = pack_fv (explode_qn (`%Mkdtuple2)) in
    (Pat_Cons mkt2_fv [(p1,false); (p2,false)], bvs @ [bv])

// Make the property that assumes the pre-condition and checks the post-condition.
val make_prop :
  bs:list binder{List.Tot.Base.length bs > 0} ->
  input_t:typ ->
  fun_id:term ->
  pre_id:term ->
  post_id:term ->
  isL:bool ->
  Tac term
let make_prop bs input_t fun_id pre_id post_id isL =
  let (pat, bvs) = make_pattern bs in
  // Fresh binder for the abstraction fun a -> match a with ...
  let fresh_b = fresh_binder (top_env()) "a" input_t in
  let b_bv = pack (Tv_BVar (fix_bv_index (get_binder_bv fresh_b) 0)) in
  // Apply binders to pre-condition identifier
  let pre_cond = apply_list_bv pre_id bvs 0 in
  let assumption = `(FStar_QCheck.assume_ (`#pre_cond)) in
  // Apply binders to post-condition identifier
  let post_cond = apply_list_bv post_id bvs 1 in
  // Here we have to distinguish between lemmas and normal functions,
  // as the latters' post-conditions also take the function's return value
  let post_cond =
    if isL
    then post_cond
    else (let r = apply_list_bv fun_id bvs 1 in
          apply_list post_cond [r])
  in
  let wild = fresh_bv_named "_" (`unit) in
  let cond = pack (Tv_Let false [] wild assumption post_cond) in
  let match_t = pack (Tv_Match b_bv None [(pat, cond)]) in
  pack (Tv_Abs fresh_b match_t)

// Make the QCheck test and the generator template.
val make_test :
  fname:string ->
  fmodule:string ->
  smodule:name ->
  t:typ ->
  is_lemma:bool ->
  default_typ:typ ->
  Tac decls
let make_test fname fmodule smodule typ isL default_typ =
  let (args, c) = collect_args typ [] in
  let bs = List.Tot.map (fun(b,_,_) -> b) args in
  let implicits = List.Tot.filter (fun b -> not (is_explicit_argument b)) bs in
  let bs = List.Tot.filter is_explicit_argument bs in
  // We check that the function has explicit arguments.
  // This makes the following assumption safe.
  guard (List.Tot.Base.length bs > 0);

  // Fully apply type arguments
  let instantiate_implicits (b:binder) : Tac binder =
    let s = get_bv_typ (get_binder_bv b) in
    let s' = fold_left
      (fun t impl -> subst (get_binder_bv impl) default_typ t)
      s
      implicits
    in fix_binder_sort b s'
  in
  let bs = map instantiate_implicits bs in
  let bs = List.Tot.rev bs in

  let qfname = (quote fname) in
  let args_typ = List.Tot.map (fun b -> get_bv_typ (get_binder_bv b)) bs in
  // TODO: Prove List.Tot.Properties.map_lemma for Tac map
  assume(List.Tot.length args_typ > 0);

  // Get the generator's type
  let input_t = binder_list_to_dtuple bs in
  let gen = `(FStar_QCheck.undefined_gen (`#input_t)
    ("Generator for " ^ (`#qfname) ^ "'s input not yet implemented")) in
  let gen_fv = pack_fv (smodule@["gen_" ^ fname ^ "_args"]) in
  let gen_id = pack (Tv_FVar gen_fv) in
  let pre_fv = pack_fv (smodule@[fname ^ "_pre"]) in
  let post_fv = pack_fv (smodule@[fname ^ "_post"]) in
  let pre_id = pack (Tv_FVar pre_fv) in
  let post_id = pack (Tv_FVar post_fv) in
  let fun_id = pack (Tv_FVar (pack_fv [fmodule; fname])) in
  let prop = make_prop bs input_t fun_id pre_id post_id isL in
  let test = `(FStar_QCheck.test_make
                 ((`#qfname) ^ "_spec")
                 (`#gen_id)
                 (`#prop)) in
  let typ = `(FStar_QCheck.test_t) in
  let lb_gen = pack_lb ({lb_fv = gen_fv;
                         lb_us = [];
                         lb_typ = (`(FStar_QCheck.arbitrary (`#input_t)));
                         lb_def = gen}) in
  let sv_gen = Sg_Let false [lb_gen] in
  let lb_test = pack_lb ({lb_fv = pack_fv (smodule@["test_" ^ fname ^ "_spec"]);
                          lb_us = [];
                          lb_typ = typ;
                          lb_def = test}) in
  let sv_test : sigelt_view = Sg_Let false [lb_test] in
  let ses : list sigelt = [  pack_sigelt sv_gen
                           ; pack_sigelt sv_test
                          ] in
  ses
