(* The MIT License (MIT)

 Copyright (c) 2020 Nomadic Labs <contact@nomadic-labs.com>

 Permission is hereby granted, free of charge, to any person obtaining
 a copy of this software and associated documentation files (the
 "Software"), to deal in the Software without restriction, including
 without limitation the rights to use, copy, modify, merge, publish,
 distribute, sublicense, and/or sell copies of the Software, and to
 permit persons to whom the Software is furnished to do so, subject to
 the following conditions:

 The above copyright notice and this permission notice shall be
 included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE. *)

// Incremental Merkle Trees

// _Incremental Merkle Trees_ are an optimization of full Merkle trees
// of fixed height.

// A full Merkle tree of height h contains 2 ^ h leaves, and h+1
// levels of nodes, with leaves at level 0 and its root at level h. In
// the Tezos codebase, we use IMTs of fixed height (32 in the sapling
// storage library for Tezos since the Edo(008) protocol). Trees of
// fixed size enable in turn proofs of membership---witnesses---, of
// fixed size.

// Unlike regular Merkle trees, Incremental Merkle Trees can be indeed
// Empty. Intuitively, an empty IMT models a complete Merkle tree
// where all leaves are pre-filled with a default value, given by
// uncommitted 0.

// IMTs are _incremental_ in the sense that leaves cannot be modified
// but only added, and they are moreover inserted exclusively in
// successive positions from left to right. IMTs then optimize both
// the size of the underlying Merkle tree and the computation of known
// hashes for default values:

// - Internally, a sub-tree filled with default values is represented
// by an Empty tree, avoiding to construct the sub-tree.

// - Since all the nodes at the same level of an underlying Empty
// sub-tree have the same hash, it can be computed from the default
// value of the leaves. This is stored in the uncommitted list, with
// index n > 0.

module Tree.Data

module C = Commitments

module UInt32 = FStar.UInt32
type int63 = Int63.t
type uint32 = UInt32.t
module Int64 = FStar.Int64
type int64 = Int64.t

val lit : i:(FStar.Int.int_t 63){i >= 0 /\ i <= Int63.v C.max_height} ->
          b:C.v_height{Int63.v b = i}
let lit i = Int63.int_to_t i

val pred63 : x:C.v_height{Int63.(x >^ lit 0)} -> C.v_height
let pred63 x = Int63.(x -^ lit 1)

val suc63 : x:int63{Int63.(x >=^ lit 0 /\ x <^ C.max_height)} -> C.v_height
let suc63 x = Int63.(x +^ lit 1)

val v63 : h:C.v_height -> Tot (n:nat{n = Int63.v h})
let v63 h = assert(Int63.(h >=^ lit 0)); Int63.v h

val pow2_32 : e:int63{Int63.(e >=^ lit 0 /\ e <^ C.max_height)} -> uint32
let pow2_32 e =
  let e32 = Int63.to_uint32 e in
  UInt32.shift_left 1ul e32

val pow2_32_correct : e:int63{Int63.(e >=^ lit 0 /\ e <^ C.max_height)} ->
  Lemma (ensures ((UInt32.v (pow2_32 e)) == pow2 (v63 e)))
        [SMTPat (pow2_32 e)]
let pow2_32_correct e =
    let e32 = UInt32.uint_to_t (v63 e) in
    FStar.UInt.shift_left_value_lemma #32 1 (v63 e);
    FStar.Math.Lemmas.pow2_lt_compat 32 (UInt32.v e32)


// Storing the height in Empty would simplify everything, but we don't
// want to do it in the key value store implementtion in the
// protocol. So, instead, we always pass the height as argument.

// 'a must be an eqtype only if we want to define a `mem` function.
type tree 'a 'b : Type =
  | Empty : tree 'a 'b
  | Leaf : 'a -> tree 'a 'b
  | Node : 'b -> l:tree 'a 'b -> r:tree 'a 'b -> tree 'a 'b


///////////////////////////
// High level interface
///////////////////////////

// n is the size of the tree and also the next position to fill
type htree a b = option uint32 * tree a b

val default_height :( h:C.v_height{Int63.(h =^ lit 32)})
let default_height = lit 32

let max_size = Int64.shift_left 1L 32ul

// This has to be defined here, because it's used by Methods and Properties
val size : ht:htree 'a 'b -> int64
let size (on, t) =
  match on with
  | None -> max_size
  | Some n -> Int.Cast.uint32_to_int64 n

open FStar.List.Tot

// U32 length
val length' :
  l:list 'a ->
  ac:uint32 ->
  uint32
let rec length' l ac=
  match l with
  | [] -> ac
  | _::xs -> length' xs (UInt32.add_mod 1ul ac)

val lem_mod_32 :
  x:nat -> y:uint32 -> z:nat{z=Int64.v max_size} ->
  Lemma ((x + UInt32.(v (add_mod 1ul y))) % z =
  	 ((x + 1) + UInt32.v y) % z)
let lem_mod_32 x y z =
  assert(z = pow2 32);
  assert(UInt32.(v (add_mod 1ul y)) = (1 + UInt32.v y) % z);
  FStar.Math.Lemmas.lemma_mod_plus_distr_r x (1 + UInt32.v y) z

val lem_length' :
  l:list 'a ->
  ac:uint32 ->
  Lemma (UInt32.v (length' l ac) = (length l + UInt32.v ac)% Int64.v max_size)
        [SMTPat (length' l ac)]
#push-options "--z3rlimit 20"
let rec lem_length' l ac =
  match l with
  | [] -> ()
  | _::xs ->
    lem_length' xs (UInt32.add_mod 1ul ac);
    lem_mod_32 (length xs) ac (Int64.v max_size);
    assert((length xs + UInt32.(v (add_mod 1ul ac)))% Int64.v max_size =
         (length xs + 1 + UInt32.v ac) %  Int64.v max_size)
#pop-options

val length32 :
  l:list 'a ->
  Tot (y:uint32{UInt32.v y = (List.Tot.length l) % Int64.v max_size})
let length32 l = length' l 0ul
