# Functional Verification of the Sapling Storage Library in F*

## Introduction
This repository contains the source code for the *Functional Verification of the Sapling Storage Library in F\** project. Let's begin this brief introduction by breaking down its name.

### Sapling
First off, [*Sapling*](https://tezos.gitlab.io/008/sapling.html) is a protocol used in Tezos that enables privacy-preserving transactions of tokens. For storage purposes, this protocol uses the *Incremental Merkle Tree* data structure, or IMT. This IMT structure is simply a fixed height Merkle tree, in which the leaves are only stored on the last level in the leftmost positions. In a later [section](#incremental-merkle-tree), we provide a more detailed description of IMTs. For more documentation, refer to the protocol's [specification](https://github.com/zcash/zips/blob/2e26bb072dfd5f842fe9e779bdec8cabeb4fa9bf/protocol/protocol.pdf).

In Tezos, there are currently two implementations of Sapling. The first one, found in `lib_sapling`, implements IMTs through purely functional ADTs. In contrast, the second implementation, which is part of the economic protocol, is written in an stateful style to make use of the protocol's flat storage. While the latter is more efficient, the former is simpler and thus, easier to verify.

### F*
[F\*](https://www.fstar-lang.org/) is an ML-style functional programming language aimed at formal verification. F\* combines automated verification through an SMT solver with the interactive functionality of a proof assistant based on dependent types, making it a hybrid verification tool. After the verification step, an F\* program can be extracted to OCaml in order to execute it.

### Project
Now that all of the project's discrete parts have been introduced, we can describe what the project is about. The general goal is to assess the feasibility of constructing an F\* formally verified specification of an OCaml data structure from which to:
  * Extract a certified and efficient OCaml implementation, and
  * Extract the specification as property based tests for the extracted implementation.

To carry this out, we take Sapling's IMTs as a case study. For the reasons mentioned in a [previous section](#sapling), we start with the `lib_sapling` implementation, for which we apply the three steps we've just presented (verification, implementation extraction and specification extraction). After finishing with it, we move on to the protocol version, as we discuss in [this section](#protocol).

## Motivation
With the *what?*'s out of the way, let's get to the *why?*'s.

The first question one might ask regarding the project is *why F\*?*, or even *why not Coq?* The answer to this has (at least) two parts:
  * Thanks to the SMT support, *F\** should automate (or at least partially automate) some proofs that may become too tedious in Coq or other interactive proof assistants.
  * There is a partial [Coq specification for IMT's](https://gitlab.com/nomadic-labs/sapling-verification/-/tree/master/experiments/ssr_trees), with which we will compare this project to assess *F\**'s benefits and disadvantages for this kind of verification.

Another likely question could be *why Sapling's IMTs?* We believe that IMTs are a good self-contained data structure that is simple enough to experiment with their verification while having sufficient non-trivial properties to make their verification worthwhile.

Finally, after reading the previous section the question of *why test something you've already proved?* might arise. Testing a certified implementation is useful because of the **verification gap**, that is, the semantic difference between the F\* model and the source OCaml implementation. This amounts to a set of assumptions that are easily overlooked. Additionally, the correctness of F\*'s extraction is also assumed, potentially alongside several properties that might be too inconvenient to prove.

An example of this verification gap is the integer model used in the specification, which might not always respect the semantics of OCaml's machine integers. This true of our specification: the source OCaml code uses OCaml's `int` type, which can be either 31 or 63 bits wide depending on the computer architecture. We model them as 63 bit integers, assuming a 64 bit system. However, if executed in a 32 bit system, the tests would be able to catch errors caused by arithmetic overflows, which couldn't be detected by the specification.

## General workflow
An outline of the general workflow evaluated in this project is:
  1. Model an OCaml program in F\*, stating and proving all the desired properties, as shown in the [IMT Verification section](#imt-verification).
  2. Extract a new (certified) OCaml implementation from the model, using F\*'s native OCaml extraction, as described in [this section](#certified-ocaml-implementation-extraction).
  3. Extract the model's specification as PBTs for the certified implementation. To do so, we implemented a Meta-F\* module called `SpecExtraction`. Briefly, its goal is to extract a function's (or lemma's) pre and post-condition as OCaml functions returning a boolean value, to then use them as QCheck properties. This module is described at length in this [issue](https://gitlab.com/nomadic-labs/sapling-verification/-/issues/21).  A summary of its functionality can also be found in [a following section](#specification-extraction-and-qcheck-integration).

TODO: an alternative workflow ...

## Case study: Sapling's Incremental Merkle Trees
### Overview
The project's source code structure is the following (excluding F\*'s cache and hints files):
```
.
├── Commitments.fsti                       ┐
├── Int63.fsti                             |
├── Tree.fst                               |
├── Tree.Data.fst                          |  IMT
├── Tree.Properties.fst                    |  model
├── Tree.Methods.fst                       |
├── Tree.InsertionIdentities.fst           |
├── FullMTree.fst                          ┘
├── SpecExtraction.fst                     ┐
├── SpecExtraction.Util.fst                |
├── SpecExtraction.PrePost.fst             |Specification
├── SpecExtraction.TestGeneration.fst      |extraction
├── Tree.PrePost.fst                       ┘
├── _out
│   ├── Tree_Data.ml                       ┐
│   ├── Tree_Methods.ml                    |
│   ├── Tree_Properties.ml                 | Extracted implementation and spec
│   ├── Tree_InsertionIdentities.ml        |
│   └── Tree_PrePost.ml                    ┘
├── ml
|   ├── Commitments.ml                     ┐ OCaml realization
|   ├── Int63.ml                           ┘ of F* interfaces
|   ├── main.ml                            ] Sample main
|   ├── Tree.ml                            ] Bundles Tree_Data and Tree_Methods
|   └── test
|       ├── generators.ml                  ┐ QCheck generators and
|       └── test_internal_tree.ml          ┘ tests
├── Makefile
├── cleanup                                ] Extracted code cleanup script
└── README.md
```

### Building
The `Makefile` requires either to have `fstar.exe` in your PATH or to define `FSTAR_HOME` to be the path to the `FStar` directory in your environment.
The available targets are: `verify`, `extract`, `test`, `build-main` and `clean`. Their usage is described in the following sections. Running
```
make
```
in the project's root directory will verify the IMT model.

F\* version used: built from source (SHA: 73c6d11a543d98d47ae926fb0e5041c41d8babca).

### Incremental Merkle Tree

An Incremental Merkle Tree of height `h` contains `2^h` leaves and `h + 1` levels of nodes, with all leaves at level `0` and root at level `h`.

For now, we focus on the IMT implementation found in the [`Storage` module](https://gitlab.com/tezos/tezos/-/blob/master/src/lib_sapling/storage.ml) from `lib_sapling`. This implementation includes the following definition of an algebraic data type `tree` to represent IMTs:
```ocaml
type tree = Empty | Leaf of C.Commitment.t | Node of (H.t * tree * tree)
```
This type of trees have *commitments* in their leaves and *hashes* (`H.t`) in their nodes. Although both of these have the same internal representation, they are differentiated because the commitments correspond to the values stored in the tree, while nodes' hashes are used to preserve integrity (they are the usual Merkle hashes).

The trees are always treated as being full, using the default value `H.uncommitted 0` for unused leaves. All the nodes at the same level of an empty tree have the same hash, which can be computed from the default value of the leaves. These hashes are stored in the `H.uncommitted` list, so `H.uncommitted n` is the hash of an empty tree of height `n`.

To avoid storing huge empty trees, any subtree filled with default values is represented by the `Empty` constructor and given its height it's possible to compute its hash using the `H.uncommitted` list. Because of this, we'll sometimes refer to these trees as *compressed trees*.

The leaves are indexed by their position `pos`, ranging from `0` to `(2^h) - 1`. The tree is incremental in the sense that leaves cannot be modified but only added and exclusively in successive positions.

### IMT verification
Our F\* model of IMTs is made up of the following modules:
  * `Commitments.fsti`: Axiomatized model of commitments and hashes. Its OCaml realization (`ml/Commitments.ml`) is just a wrapper for the corresponding functions in `lib_sapling`.
  * `Int63.fsti`: Axiomatized model of OCaml's built-in int type (assuming a 64 bit architecture). This module shares the same interface as F\* standard library machine integer modules. Its OCaml realization is `ml/Int63.ml`.
  * `Tree.fst`: Implementation and verification of Incremental Merkle Trees, i.e. the `Tree` module from `lib_sapling/storage.ml`. This module is split into the following sub-modules:
    - `Tree.Data.fst`: Data structures used by the spec.
    - `Tree.Properties.fst`: Predicates and proofs used in the specification that shouldn't be part of the extracted implementation. This module is extracted for use in tests.
    - `Tree.Methods.fst`: Functions and proofs that, along with `Tree.Data`, make up the certified extracted implementation.
    - `Tree.Insertion.Identities.fst`: The `Tree` module defines an optimized insertion function `insert_list`. This modules introduces two alternative definitions for insertion, `insert_model` and `insert_pow`. Additionally, we prove all three to be equivalent.
  * `FullMTree.fst`: Implementation and verification of full Merkle trees, inspired by this [example](https://github.com/FStarLang/FStar/blob/master/examples/data_structures/MerkleTree.fst). The goal of this module is to show that every compressed IMT as defined in the `Tree.Data` module can be translated into a full Merkle tree.


For more discussion about these modules, refer to the [Sapling verification meta-issue](https://gitlab.com/nomadic-labs/sapling-verification/-/issues/16).

To verify all these modules, run:
```
make verify
```

### Certified OCaml implementation extraction
For the certified OCaml implementation, we need to extract the `Tree.Data` and `Tree.Methods` modules.
The module `ml/Tree.ml` bundles these two extracted modules, and should replace the original OCaml implementation.
To perform the extraction, we just ask F\* to extract these modules to OCaml, which can simply be done by running:
```
make extract
```
This will generate the OCaml files in `_out/` containing the extracted code (alongside the [extracted specification](#imt-specification-extraction)). For now, this extraction has some shortcomings, discussed in this [issue](https://gitlab.com/nomadic-labs/sapling-verification/-/issues/22). To work around them, we provide the `cleanup` script, that is automatically run by the Makefile.

To build the extracted code alongside the sample main (`ml/main.ml`), run:
```
make build-main
```
This will generate an executable in the `_out` directory.

### Specification extraction and QCheck integration
The final piece of this puzzle is the extraction of the model's specification as properties for property based testing.

To solve this, we introduce a Meta-F\* tactic called `extract_spec`, which is defined in the `SpecExtraction` module.
This tactic does two things. First, it extracts a function's pre and post-conditions as OCaml boolean functions by calling the `pre_post` tactic.
Second, it synthesizes the QCheck boilerplate code for defining the test.

A thorough description of how the first step works, and some difficulties we encountered while building it, can be found in [this issue](https://gitlab.com/nomadic-labs/sapling-verification/-/issues/21).
Its main functionality consists of extracting a function or lemma pre and post-conditions as OCaml boolean functions. For instance, given the following F\* function:
```fsharp
val foo : x:T{P1 x} -> Pure T' (requires (P2 x)) (ensures (fun y -> Q x y))
let foo x = ...
```
One can call the `extract_spec` tactic through a splice by adding the following line (anywhere foo is in scope):
```
%splice[] (extract_spec (`%foo))
```
As a result, in the extracted OCaml code of the module where the splice was called, the following predicates will be defined:
```ocaml
let foo_pre = fun x -> P1 x && P2 x
let foo_post = fun x -> fun y -> Q x y
```

As shown in this example, a function's pre-condition is parametrized by the function's arguments, while its post-condition takes as an additional argument the function's return value.

The second step, generating the QCheck tests, is performed by the `make_test` tactic (called by `extract_spec`). Continuing with the example, this tactic generates the following declarations:
```ocaml
let (gen_foo_args : T FStar_QCheck.arbitrary)
  = FStar_QCheck.undefined_gen "Generator for foo's input not yet implemented"

let (test_foo_spec : FStar_QCheck.test_t) =
  FStar_QCheck.test_make "foo_spec" gen_foo_args
    (fun a0 ->
      match a0 with
      | x -> (FStar_QCheck.assume_ (foo_pre x);
              foo_post x (foo x)))
```
The first of these corresponds to a template definition of the QCheck generator for `foo`'s arguments. Deriving the generators is outside the scope of this project. Some discussion regarding this can be found in this [issue](https://gitlab.com/nomadic-labs/sapling-verification/-/issues/20).

The second is the definition of the QCheck test, in which the pre-condition is assumed and the post-condition is checked. The `FStar_QCheck` module is just a simple interface wrapper to expose some `QCheck` functions in F\*.

Some additional discussion of this test generation tactic can be found on this [MR](https://gitlab.com/nomadic-labs/sapling-verification/-/merge_requests/20).

#### IMT Specification extraction
In our concrete case, we put all the splices in a new module called `Tree.PrePost.fst`. This is not only helpful for organizing the extracted code, but also allows the user to verify the IMT model without running the specification extraction tactic (which can be slow). These splices are executed by running:
```
make extract
```
In turn, this will generate the OCaml file `_out/Tree_PrePost.ml`, containing the pre and post-conditions and tests spliced with the `extract_spec` tactic.

#### IMT QCheck integration
Finally, we want to actually test the certified implementation against the extracted properties. To do so, we first complete the definition of the extracted QCheck generator templates, which can be found in the `ml/generators.ml`. Next, we copy the extracted QCheck tests in `ml/test_internal_tree.ml`. For instance, one of these tests is:
```ocaml
let (test_insert_model_spec : FStar_QCheck.test_t) =
  FStar_QCheck.test_make "insert_model_spec" gen_insert_model_args
    (fun a0 ->
       match a0 with
       | Prims.Mkdtuple2 (v, Prims.Mkdtuple2 (h, t)) ->
           (FStar_QCheck.assume_ (insert_model_pre v h t);
            insert_model_post v h t
              (Tree_InsertionIdentities.insert_model v h t)))
```

To compile the tests, just run:
```
make test
```
This will generate an executable called `test.exe` in the `_out` directory, which runs the tests.

### Protocol Sapling Implementation
As mentioned before, a more complex implementation of Sapling can be found in the Tezos economic protocol (`lib_protocol/sapling_storage.ml`). The increase in complexity is mainly due to the representation of IMTs used: instead of simple ADTs, this implementation stores the trees in the protocol's flat context (which is a key-value store). This is essential to reduce the memory footprint of the program, as the context is stored on disk. Additionally, this implementation uses the Lwt promise library, necessary to handle the IO operations, which adds further complexity.

This piece of code is perfect to illustrate a more general scenario. It is often the case that the source OCaml code is heavily optimized to run in a high performance application. These optimizations impose layers of complexity that are frequently orthogonal to the program's core functionality, as it is the case with the context and Lwt manipulation in our example. As a result, the [workflow](#general-workflow) we outlined might not be suitable for two reasons:
* Introducing those layers of complexity to the formal model could significantly hinder the verification effort, or be outright unreasonable (as is the case of modelling Lwt in F\* for the sake of specifying Sapling).
* Replacing high performance code with the extracted implementation might be unfeasible, at least until the F\* extraction is improved (see [this issue](https://gitlab.com/nomadic-labs/sapling-verification/-/issues/22)).

These complications don't stop at formal verification. Performing property based testing over these complex implementations can also become a difficult task, as defining the properties on the efficient data representation might not be trivial.

To address these issues we propose the following variation of the general workflow:
  1. Specify a model of the complex OCaml program in F\*, abstracting away any source of complexity that is not essential to the program's core functionality. In our case, the `lib_sapling` specification described in the previous sections is an adequate model for the `lib_protocol` implementation.
  2. Extract the model's certified OCaml implementation. Although it won't be used to replace the original code, it will be useful in the following steps.
  3. Extract the model's specification as PBTs for the extracted implementation. Testing the extracted implementation against the specification removes the reliance on the unverified extraction mechanism, ensuring that the extracted implementation satisfies the specification.
  4. Test the original implementation against the extracted code. To do so, it might be necessary to define a translation function from the optimized representation to the model. Here, the value of steps 2 and 3 is realized. Having a certified reference implementation allows one to PBT the original implementation with minimal effort. In the following section we give a detailed account of this step in our example.

#### Testing the Protocol Sapling Implementation
For the reasons explained in the previous section, we decided to test the protocol Sapling implementation against the certified and tested extracted ADT implementation. This is implemented on the `ml/test/proto-test.ml` module.
Several tests were considered, which are discussed in [this issue](https://gitlab.com/nomadic-labs/sapling-verification/-/issues/23).

The key to carrying this out was to define a projection function (`project_tree`) from the context tree representation to the IMT ADT. With this, we can describe the test we implemented with the following diagram:
```
   Protocol    |   ADT (lib_sapling)
             proj0
     t0  ──────|──────► t0'
      │                  │
insert│        |         │insert'
      │                  │
      ▼        |         ▼
     t1  ─────────────► t1'
             proj1
               |
```
This diagram is divided in two by the vertical dotted line, that delimits the protocol IMTs to the ADT IMTs. For instance, t0 stands for a context in which an IMT is stored, following the representation defined in `sapling_storage.ml`.

Here, `proj0` and `proj1` are calls to the projection function, and `insert'` is the IMT insertion function from our ADT spec (`insert_list`).

The function that we want to test is `insert`, the protocol IMT insertion (actually called `add`). Our tests consists of checking the commutativity of this diagram. This means, to check that, for any given `t0`, `proj1 (insert t0)` is equal to `insert' (proj0 t0)`.

By performing this test we ensure that the complex protocol insertion function is equivalent to the one we know is well-behaved, modulo the projections.