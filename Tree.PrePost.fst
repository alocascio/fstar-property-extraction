module Tree.PrePost

open SpecExtraction
open Tree
open Tree.InsertionIdentities

%splice[] (extract_spec (`%lemma_tree_leaves_bound))

%splice[] (extract_spec (`%lemma_length))

%splice[] (extract_spec (`%insert_model))

%splice[] (extract_spec (`%insert_pow))

%splice[] (extract_spec (`%insert_list))
