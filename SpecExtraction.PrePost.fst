module SpecExtraction.PrePost

open FStar.InteractiveHelpers.ExploreTerm

open FStar.Tactics
open FStar.InteractiveHelpers.Base
open SpecExtraction.Util

// Get refinement from a type, with its binder.
let rec get_refinement (t:typ) (bs:list binder) : Tac (option (binder * typ * term)) =
  match inspect t with
  | Tv_Refine bv' ref -> begin
    let new_b = pack_binder bv' Q_Explicit [] in
    match inspect_bv bv' with
      | {bv_ppname= nm' ; bv_index = ib;bv_sort = sort} -> begin
        // Here we may have a nested refinement, so we make a recursive call
        match get_refinement sort bs with
        | Some (i_b, ground, i_ref) ->
          // If there was some inner refinement, we join it with the outer one,
          // and unify the binder.
          let bv' = fix_bv_sort bv' ground in
          let new_b' = pack_binder (bv') Q_Explicit [] in
          let i_ref' = apply_list_bv i_ref
                       (List.Tot.map get_binder_bv (new_b' :: bs)) 0 in
          let ref' = add_binders (new_b' :: bs) (`((`#i_ref') && (`#ref))) in
          (Some (new_b', ground, ref'))
        | None ->
           let ref' = add_binders (new_b :: bs) ref in (Some (new_b, sort, ref'))
        end
    end
  | _ -> None


#push-options "--z3rlimit 10"
// Collect the arguments of a function type. For every argument, we keep its binder,
// type and potentially a refinement.
// This function returns a list of arguments alognside the final computation of the
// type.
val collect_args :
  t:typ ->
  ac:list (binder * typ * option term) ->
  Tac (l:list (binder * typ * option term){List.Tot.length l > 0} * comp)
let rec collect_args t ac =
  // Unify binders from arrow and refinement.
  let fix_binder_ref
    (b_arrow:binder)
    (b_ref:binder)
    (ref:term)
    (bs:list binder)
    : Tac (binder * term)
    = let b_arrow' = fix_binder_sort b_arrow (get_bv_typ (get_binder_bv b_ref)) in
      let applied_ref = apply_list_bv ref
                          (List.Tot.map get_binder_bv (b_arrow' :: bs)) 0 in
      let ref' = add_binders (b_arrow' :: bs) applied_ref in
      (b_arrow', ref')
  in
  // Base case: [typ] is of the form [b -> c].
  let base_case
    (b:binder)
    (ac:list (binder * typ * option term))
    (c:comp)
    : Tac (l:list (binder * typ * option term){List.Tot.length l > 0} * comp)
    = let b_type = get_bv_typ (get_binder_bv b) in
      let prev_binders = List.Tot.map (fun(x,_,_) -> x) ac in
      match get_refinement b_type prev_binders with
      | Some (b', _, ref) ->
        let (b'', ref') = fix_binder_ref b b' ref prev_binders in
        ((b, b_type, Some ref') :: ac, c)
      |  None ->
        ((b, b_type, None) :: ac, c)
  in
  match inspect t with
  | Tv_Arrow b c -> begin
    match inspect_comp c with
    | C_Total ret _ -> begin
      match inspect ret with
      | Tv_Arrow _ _ -> begin //Recursive case: get binder, ground and ref
        let b_type = get_bv_typ (get_binder_bv b) in
        let prev_binders = List.Tot.map (fun(x,_,_) -> x) ac in
        match get_refinement b_type prev_binders with
        | Some (b', _, ref) ->
          let (b'', ref') = fix_binder_ref b b' ref prev_binders in
          collect_args ret ((b, b_type, Some ref') :: ac)
        | None -> collect_args ret ((b, b_type, None) :: ac)
        end
      | _ -> base_case b ac c
      end
    | _ -> base_case b ac c
    end
  | _ -> fail "Not an arrow"
#pop-options

// Fold a list of arguments by joining (&&) the refinements and constructing
// the final pre-condition's type.
let rec fold_preconditions
  (binders:list (binder * typ * option term))
  (ac_typ:typ)
  (ac_term:term)
  (n_binders: nat)
  : Tac (typ * term)
  = match binders with
    | [] -> (ac_typ, ac_term)
    | ((b,ty,None)::rest) ->
      let ac_typ' = pack (Tv_Arrow b (pack_comp (C_Total ac_typ []))) in
      fold_preconditions rest ac_typ' ac_term (n_binders + 1)
    | ((b,ty,Some ref)::rest) ->
      let bs =  map (fun(x,_,_) -> get_binder_bv x) binders in
      let ref' = apply_list_bv ref bs n_binders in
      let ac_term' = `((`#ref') && (`#ac_term)) in
      let ac_typ' = pack (Tv_Arrow b (pack_comp (C_Total ac_typ []))) in
      fold_preconditions rest ac_typ' ac_term' (n_binders + 1)

// Join the preconditions extracted from the arguments' refinements.
val join_preconditions :
  binders:list (binder * typ * option term) ->
  Tac (typ * term)
let join_preconditions binders =
  let (ty, ref) = fold_preconditions binders (`bool) (`true) 0 in
  let ref' = add_binders (map (fun(x,_,_) -> x) binders) ref in
  (ty, ref')

// Remove [&& true] generated when unfolding pre-conditions
val remove_and_true : t:term -> Tac term
let remove_and_true t =
  let remove_and_true_v
    (t:term)
    : Tac term
    = match inspect t with
      | Tv_App e (a,q) -> begin
        match inspect e with
        | Tv_App hd (b,q') ->
          if (compare_term hd (`Prims.op_AmpAmp) = FStar.Order.Eq &&
              compare_term a (`true) = FStar.Order.Eq)
          then b
          else t
        | _ -> t
        end
      | _ -> t
  in
  visit_tm remove_and_true_v t

// Join two parametrized conditions with (&&). To do so, we have to fully apply
// both using a list of binders, and after joining them, abstract all the variables
// back.
val join_deep :
  t1:term ->
  t2:term ->
  bs:list binder ->
  Tac term
let join_deep t1 t2 bs =
  let bvs =  List.Tot.map get_binder_bv bs in
  let t1' = apply_list_bv t1 bvs 0 in
  let t2' = apply_list_bv t2 bvs 0 in
  let conj = `((`#t1') && (`#t2')) in
  add_binders bs conj

// Recover the postcondition's type from the list of arguments.
let rec post_type
  (binders:list binder)
  (ac_typ:typ)
  : Tac typ
  = match binders with
    | [] -> ac_typ
    | (b::rest) ->
      let ac_typ' = pack (Tv_Arrow b (pack_comp (C_Total ac_typ []))) in
      post_type rest ac_typ'

// Extract the (pre and) post-condition from the final computation, alongside
// the latter's type.
// The boolean flag indicates whether the type is a lemma.
val get_final_comp :
  c:comp ->
  bs:list binder ->
  Tac (option term * typ * term * bool)
let get_final_comp c bs =
  // Retrieve post-condition from the ensures clause.
  let get_post
    (t:term)
    (bs:list binder)
    (ty:typ)
    : Tac (binder * term)
    = match inspect t with
      | Tv_Abs b t ->
        (b, add_binders (b::bs) t)
      | _ ->
        let b = fresh_binder (top_env()) "r" ty in
        (b, add_binders (b::bs) t)
  in
  match inspect_comp c with
  | C_Total ret _ -> begin
    match get_refinement ret bs with
    | None ->
      let b = fresh_binder (top_env()) "r" ret in
      (None, post_type (b::bs) (`bool), add_binders (b::bs) (`true), false)
    | Some (b, _, ref) ->
      (None, post_type (b::bs) (`bool), ref, false)
    end
  | C_Eff _ _ ret [post,_] -> begin
    match get_refinement ret bs with
    | None ->
      let (b, p) = get_post post bs ret in
      (None, post_type (b::bs) (`bool), p, false)
    | Some (b_ref, _, ref) ->
      let (b_post, p) = get_post post bs ret in
      let post' = join_deep ref p (b_post::bs) in
      (None, post_type (b_post::bs) (`bool), post', false)
    end
  | C_Eff _ _ ret [pre,_;post,_] -> begin
    let pre' = add_binders bs pre in
    match get_refinement ret bs with
    | None ->
      let (b, p) = get_post post bs ret in
      (Some pre', post_type (b::bs) (`bool), p, false)
    | Some (b_ref, _, ref) ->
      let (b_post, p) = get_post post bs ret in
      let post' = join_deep ref p (b_post::bs) in
      (Some pre', post_type (b_post::bs) (`bool), post', false)
    end
  | C_Lemma pre post _ -> begin
    let pre' = add_binders bs pre in
    // Need to unthunk the post-condition
    let post' = `((`#post) ()) in
    let post' = add_binders bs post' in
    (Some pre', post_type bs (`bool), post', true)
    end
  | _ -> fail "Unexpected comp"


// This is the main tactic: we lookup the definition of an identifier
// and compute its pre and post-conditions with their types.
// The boolean flag indicates whether the type is a lemma.
val pre_post :
  typ ->
  Tac (term * typ * term * typ * bool)
let pre_post typ =
  // We collect the arguments, and join the resulting preconditions
  let (args, c) = collect_args typ [] in
  let (args_pre_type, args_pre) = join_preconditions args in
  let bs = List.Tot.map (fun (x,_,_) -> x) args in
  // Finally, we retrieve the final computation conditions, joining the
  // precondition if needed.
  let (pre, pre_type, post, post_type, isL) =
    (match get_final_comp c bs with
    | (None, post_type, post, isL) -> (args_pre, args_pre_type, post, post_type, isL)
    | (Some comp_pre, post_type, post, isL) ->
      let pre = join_deep args_pre comp_pre bs in
      (pre, args_pre_type, post, post_type, isL)) in
  let pre = remove_and_true pre in
  (pre, pre_type, post, post_type, isL)
