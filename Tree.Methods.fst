module Tree.Methods

module UInt32 = FStar.UInt32
module Int64 = FStar.Int64

module C = Commitments
open Tree.Data
open Tree.Properties

///////////////////
// Internal API
//////////////////

val to_list : tree 'a 'b -> list 'a
let rec to_list = function
  | Empty -> []
  | Leaf v -> [v]
  | Node _ l r -> (to_list l) @ (to_list r)


val lemma_to_list_nil : #a:eqtype -> #b:Type -> t:tree a b ->
    Lemma (requires (valid t))
          (ensures (not (Empty? t) <==> to_list t <> []))
let rec lemma_to_list_nil #a #b = function
  | Empty -> ()
  | Leaf v -> ()
  | Node _ l r -> lemma_to_list_nil l ; lemma_to_list_nil r

val lemma_length : #a:eqtype -> #b:Type -> t:tree a b ->
    Lemma (requires true)
          (ensures (count_leaves t = List.Tot.length (to_list t)))
          [SMTPat (count_leaves t)]
let rec lemma_length #a #b = function
  | Empty -> ()
  | Leaf v -> ()
  | Node _ l r ->
    lemma_length l;
    lemma_length r;
    List.Tot.append_length (to_list l) (to_list r)

// Gets the root hash at a certain [height]. If the tree is [Empty],
// we use the [uncomitted] list to get the corresponding hash.
val get_root_height : t:tree C.t_C C.t_H -> h:C.v_height ->
  Pure C.t_H (requires (valid t /\ has_height h t))
             (fun h' -> h' = Properties.get_root_height t h)
let get_root_height tree height =
  //assert (Compare.Int.(height >= 0 && height <= 32)) ;
  match tree with
  | Empty ->
      C.uncommitted height
  | Node h _ _ ->
      h
  | Leaf h ->
      C.hash_of_commitment h

// Apply the [merkle_hash] function over the root hashes of
// the subtrees [t1] and [t2].
val hash : h:C.v_height ->
  t1:tree C.t_C C.t_H{(valid t1 /\ has_height h t1)} ->
  t2:tree C.t_C C.t_H{(valid t2 /\ has_height h t2)} ->
  Pure C.t_H
       (requires true)
       (fun hash -> hash = Properties.hash h t1 t2)
let hash height t1 t2 =
  C.merkle_hash
    height
    (get_root_height t1 height)
    (get_root_height t2 height)

val get : #a:eqtype -> #b:Type -> h:C.v_height ->
    t:tree a b {incremental h t} ->
    pos:uint32 {UInt32.v pos < count_leaves t} ->
    Tot (o:a {List.Tot.index (to_list t) (UInt32.v pos) = o})
        (decreases %[v63 h])
let rec get #a #b h t pos = match t with
  | Leaf v -> v
  | Node _ l r ->
    let hs = pred63 h in
    max_leaves hs l;
    max_leaves_full hs l;
    if UInt32.(pos <^ pow2_32 hs)
    then (
      lemma_index_prefix (to_list l) (to_list r) (UInt32.v pos);
      get hs l pos)
    else (
      lemma_index_postfix (to_list l) (to_list r) (UInt32.v pos);
      get hs r UInt32.(pos -^ pow2_32 hs))


// The next optimization allows for eficient batching insertion of
// consecutive commitments, i.e., it will insert a list of
// commitments, one next to the other, if there is enough space
// available in the tree.
open FStar.List.Tot

val split_at :
  #a:eqtype ->
  p:uint32 ->
  l1:list a ->
  Pure (list a * list a)
       (requires True)
       (ensures (fun(l2, l3) ->
         (l2 @ l3 = l1) /\
         (UInt32.v p > length l1 ==> l2 = l1 /\ l3 = []) /\
         (UInt32.v p <= length l1 ==>
           length l2 = UInt32.v p /\ length l3 = length l1 - UInt32.v p)))
       (decreases l1)
let rec split_at #_ p l =
  if UInt32.(p =^ 0ul) then ([], l)
  else
    match l with
    | [] -> ([], l)
    | x :: xs ->
       let (l1, l2) = split_at UInt32.(p -^ 1ul) xs in
       (x :: l1, l2)


val insert_list :
  vs:list C.t_C ->
  pos:uint32 ->
  h:C.v_height ->
  t:(imt h) ->
  Pure (imt h)
       (requires (length vs <= pow2 (v63 h) - UInt32.v pos
                  && UInt32.v pos = count_leaves t))
       (ensures (fun t' ->  to_list t' = to_list t @ vs))
       (decreases %[v63 h;0])
val insert_node :
  h:int63{Int63.(h >=^ lit 0 && h <^ C.max_height)} ->
  l:imt h ->
  r:imt h ->
  pos:uint32 ->
  vs:list C.t_C {length vs > 0} ->
  Pure (imt (suc63 h))
       (requires (left_leaning (Node (hash h l r) l r)
                  && count_leaves l + count_leaves r = UInt32.v pos
                  && UInt32.v pos < pow2 (v63 (suc63 h))
                  && length vs <= (pow2 (v63 (suc63 h))) - UInt32.v pos))
       (ensures (fun t' -> to_list t' = to_list l @ to_list r @ vs))
       (decreases %[v63 h;1])

#push-options "--z3rlimit 50"
let rec insert_list vs pos h t =
  match (t, Int63.(h =^ lit 0), vs) with
  | (Empty, true, v::[]) ->
        Leaf v
  | (_, _, []) -> t
  | (Empty, _, _) ->
        insert_node (pred63 h) Empty Empty pos vs
  | (Node _ l r, _, _) ->
        max_leaves_full (pred63 h) l;
        List.Tot.append_assoc (to_list l) (to_list r) vs;
        insert_node (pred63 h) l r pos vs
and insert_node h l r pos vs =
  max_leaves h l;
  max_leaves_full h l;
  let (l', r') =
    if UInt32.(pos <^ pow2_32 h)
    then
      let at = UInt32.(pow2_32 h -^ pos) in
      let vsl, vsr = split_at at vs in
      let l' = insert_list vsl pos h l in
      let pos' = 0ul in
      let r' = insert_list vsr pos' h r in
      lemma_to_list_nil r';
      List.Tot.append_l_nil (to_list l);
      List.Tot.append_assoc (to_list l) vsl vsr;
      (l', r')
    else
      let pos' = UInt32.(pos -^ pow2_32 h) in
      let r' = insert_list vs pos' h r in
      (l, r')
  in
  max_leaves_full h l';
  let ha = hash h l' r' in
  Node ha l' r'
#pop-options

// Batch insertion post-condition as stated in Coq (imt_model.v).
// It trivially holds from [insert_list] spec.
val iter_insert_post :
  vs:list C.t_C ->
  pos:uint32 ->
  h:C.v_height ->
  t:imt h ->
  Lemma (requires (length vs <= pow2 (v63 h) - UInt32.v pos
                   /\ UInt32.v pos = count_leaves t) )
        (ensures
          (let t' = insert_list vs pos h t in
          (forall j. get h t j =
          (if (count_leaves t <= UInt32.v j && UInt32.v j < count_leaves t + length vs)
           then List.Tot.index vs (UInt32.v j - count_leaves t)
           else get h t j))))
let iter_insert_post vs pos h t = ()

///////////////////////////
// High level interface
///////////////////////////

val empty : ht:htree C.t_C C.t_H {hvalid ht /\ hmerkle ht}
let empty = (Some 0ul , Empty)

let hto_list (n, t) = to_list t

// [hget] takes an int64 for the pos to get, and casts it into an uint32 when
// needed. We also could directly take an uint32, requiring the client to do the
// casting.
val hget :
  #a:eqtype -> #b:Type ->
  pos:int64 ->
  ht:htree a b{hvalid ht} ->
  Pure (option a)
       (requires True)
       (ensures (fun o ->
          (Int64.(pos >=^ 0L) ==> List.Tot.nth (hto_list ht) (Int64.v pos) = o) /\
	  (Int64.(pos <^ 0L) ==> None? o)))
#push-options "--z3rlimit 100"
let hget #a #b pos (os,t) =
  lemma_nth_index (to_list t);
  match (os, Int64.(pos <^ 0L || pos >=^ max_size)) with
  | _, true -> if Int64.(pos >=^ max_size)
       	      then assert(Int64.v pos >= (length (to_list t)))
	      else ();
	      None
  | None, _ -> Some (get default_height t (FStar.Int.Cast.int64_to_uint32 pos))
  | Some s, _ ->
    let pos' = (FStar.Int.Cast.int64_to_uint32 pos) in
    assert(Int64.(pos >=^ 0L));
    assert(Int64.(pos <^ max_size));
    if UInt32.(pos' <^ s)  then
    (
      max_leaves default_height t;
      lem_default ();
      assert(List.Tot.nth (to_list t) (Int64.v pos) =
             Some (get default_height t pos'));
      Some (get default_height t pos')
    )
    else
      None
#pop-options

val add : v:C.t_C -> ht:himt{not (hfull ht)} ->
    ht':himt{hto_list ht' = hto_list ht @ [v] }
#push-options "--z3rlimit 30"
let add v (os, t) =
  let (Some s) = os in
  max_leaves_full default_height t;
  max_leaves default_height t;
  lem_default ();
  let t' = insert_list [v] s default_height t  in
  if UInt32.(s =^ lognot 0ul)
  then (
    max_leaves_full default_height t';
    (None, t'))
  else (Some (UInt32.add s 1ul), t')
#pop-options

val add_list :
  vs:list C.t_C ->
  ht:himt{Int64.v (size ht) <= Int64.v max_size - length vs} ->
  ht':himt{hto_list ht' = hto_list ht @ vs}
#push-options "--z3rlimit 30"
let add_list vs (os, t) =
  match vs with
  | [] -> (os, t)
  | _ ->
    let (Some s) = os in
    lem_default();
    let lvs = length32 vs in
    let t' = insert_list vs s default_height t in
    if UInt32.(add_mod s lvs =^ 0ul)
    then (
      lem_add_length s vs;
      lem_mod_0 (UInt32.v s + length vs) (Int64.v max_size);
      max_leaves_full default_height t';
      lem_max_size ();
      (None, t')
    )
    else (
      lem_add_length s vs;
      lem_mod_pos (UInt32.v s + length vs) (Int64.v max_size);
      lem_max_size();
      FStar.Math.Lemmas.small_modulo_lemma_1 (length vs) (Int64.v max_size);
      (Some (UInt32.add s lvs), t'))
#pop-options

open FStar.Classical
val hget_left_leaning : #a:eqtype -> #b:Type -> ht:htree a b ->
  Lemma (requires (hvalid ht))
        (ensures (forall (i:int64{Int64.(i >=^ 0L)}) (j:int64{Int64.(i >=^ 0L)}).
                 (None? (hget i ht) /\ Int64.(i<^j)) ==> None? (hget j ht)))
let hget_left_leaning #a #b ht =
  let aux (i:int64{Int64.(i >=^ 0L)})
          (j:int64{Int64.(i >=^ 0L)})
	  : Lemma (requires (None? (hget i ht) /\ Int64.(i<^j)))
	          (ensures (None? (hget j ht)))
          = if Int64.(j >=^ max_size) then ()
	    else (
	      assert(Int64.(i <^ max_size));
	      list_left_leaning (hto_list ht)
	    )
  in
  let aux' i = move_requires (aux i) in
  forall_intro_2 aux'
