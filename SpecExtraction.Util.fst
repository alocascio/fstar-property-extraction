module SpecExtraction.Util

open FStar.InteractiveHelpers.ExploreTerm
open FStar.Tactics
open FStar.InteractiveHelpers.Base

// Selectors and modifiers for binders
let get_bv_typ (bv:bv) : Tot typ =
 match inspect_bv bv with
      | {bv_ppname= _ ; bv_index = _;bv_sort = sort} -> sort

let get_bv_name (bv:bv) : Tot string =
 match inspect_bv bv with
      | {bv_ppname= nm ; bv_index = _;bv_sort = _} -> nm

let get_binder_bv (b:binder) : Tot bv =
  let bv,_ = inspect_binder b in bv

let fix_bv_index (b:bv) (i:int) : Tot bv =
   match inspect_bv b with
   | {bv_ppname= nm ; bv_index = _;bv_sort = s} ->
     pack_bv ({bv_ppname= nm ; bv_index = i;bv_sort = s})

let fix_bv_sort (b:bv) (ty:typ) : Tot bv =
   match inspect_bv b with
   | {bv_ppname= nm ; bv_index = i;bv_sort = s} ->
     pack_bv ({bv_ppname= nm ; bv_index = i;bv_sort = ty})

let fix_binder_sort (b:binder) (ty:typ) : Tot binder =
  let (bv, (q,l)) = inspect_binder b in
  let bv' = fix_bv_sort bv ty in
  pack_binder bv' q l

// Add a list of binders to a term
let rec add_binders (bs:list binder) (t:term) : Tac term =
  match bs with
  | [] -> t
  | b::rest ->
    add_binders rest (pack (Tv_Abs b t))

// Apply a list of terms to a function.
val apply_list :
  f:term ->
  l:list term ->
  Tac term
let apply_list f l =
  List.Tot.fold_right
    (fun b g -> `((`#g) (`#b))) l f

// Apply a list of variables to a function, taking care of the bv's index.
val apply_list_bv :
  f:term ->
  l:list bv ->
  offset:nat ->
  Tac term
let apply_list_bv f l offset =
  let indexed = fst (
    List.Tot.fold_left
      (fun (bs, i) b -> ((b, i - 1)::bs, i - 1))
      ([], List.Tot.length l + offset)
      ( List.Tot.rev l)) in
  let l' = map (fun (b, i) -> pack (Tv_BVar (fix_bv_index b i))) indexed in
  apply_list f l'

// Get the return type of a function type.
val return_type : t:typ -> Tac typ
let rec return_type t =
  match inspect t with
  | Tv_Arrow b c -> begin
    match inspect_comp c with
    | C_Total ret _ -> return_type ret
    | C_Eff _ _ ret _ -> return_type ret
    | _ -> fail "Unexpected comp"
    end
  | _ -> t

// Remove [b2t] calls, adapting the equality operator if needed.
// TODO: generalize, translate /\ to && and so on.
let remove_b2t (t:term) : Tac term =
  match t with
  | Tv_App hd (a,q) ->
    if compare_term hd (`b2t) = FStar.Order.Eq
    then a
    else (if compare_term hd (`Prims.equals) = FStar.Order.Eq ||
             compare_term hd (`Prims.eq2) = FStar.Order.Eq ||
             compare_term hd (`Prims.((===))) = FStar.Order.Eq
          then pack (Tv_App (`Prims.op_Equality) (a,q))
          else t)
  | _ -> t

// Determine if a type is Type.
// Fix this! How?
let is_type (t:typ) : Tac bool =
  (compare_term t (`Type0) = FStar.Order.Eq) ||
  (compare_term t (`Type) = FStar.Order.Eq) ||
  (compare_term t (`eqtype) = FStar.Order.Eq)


// Type synonym resolution
val resolve_type_synonyms_visitor : t:typ -> Tac typ
val resolve_type_synonyms : t:typ -> Tac typ
let rec resolve_type_synonyms_visitor t =
  match inspect t with
  | Tv_FVar fv -> begin
    match lookup_typ (top_env ()) (inspect_fv fv) with
    | Some r -> begin
      match inspect_sigelt r with
      | Sg_Let _ lbs -> begin
        let lbv = lookup_lb_view lbs (inspect_fv fv) in
        let ty, def = lbv.lb_typ, lbv.lb_def in
        (if is_type (return_type ty)
        then (if (compare_term t (`eqtype) <> FStar.Order.Eq)
              then resolve_type_synonyms def
              else t)
        else t)
        end
      | _ -> t
      end
    | _ -> fail "Unknown FVar"
    end
  | _ -> t
and resolve_type_synonyms t =
  let t = visit_tm remove_b2t t in
  visit_tm resolve_type_synonyms_visitor t

val erase_type : typ -> Tac typ
val erase_type_visitor : typ -> Tac typ
let rec erase_type_visitor t =
  match inspect t with
  | Tv_Refine bv _ -> erase_type (get_bv_typ bv)
  | _ -> t
and erase_type t = visit_tm erase_type_visitor t


val is_explicit_argument : b:binder -> Tot bool
let is_explicit_argument b =
  match inspect_binder b with
  | (_, (Q_Explicit, _)) -> true
  | _ -> false
