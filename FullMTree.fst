module FullMTree

open Tree
module C = Commitments
open FStar.Constructive
module List = FStar.List.Tot

// Full Merkle Trees of fixed height.
// Implementtion of full Merkle trees, heavily inspired by
// https://github.com/FStarLang/FStar/blob/master/examples/data_structures/MerkleTree.fst ([1]).

// The goal of this module is to show that every compressed Incremental Merkle Tree
// as defined in the Tree module can be translated into a full Merkle tree.
// In other words, show that the [incremental] and [merkle] predicates from
// the Tree module are sufficient for determining that a tree is indeed an IMT.

let lit = Int63.int_to_t
let suc63 = Int63.add (lit 1)
val pred63 : x:C.v_height{Int63.(x >^ lit 0)} -> C.v_height
let pred63 x = Int63.(x -^ lit 1)
let isz63 x = Int63.(x =^ lit 0)

// Complete Merkle trees, indexed by the tree's height and node's hash.
type fmtree : C.v_height -> C.t_H -> Type =
  | L: c:C.t_C -> fmtree (lit 0) (C.hash_of_commitment c)
  | N: #n:C.v_height{Int63.lt n C.max_height} -> #h1:C.t_H -> #h2:C.t_H ->
       left:fmtree n h1 ->
       right:fmtree n h2 ->
       fmtree (suc63 n) (C.merkle_hash n h1 h2)

val to_list_fmtree :
  #h:C.v_height ->
  #hash:C.t_H ->
  t:fmtree h hash ->
  Pure (list C.t_C)
       (requires True)
       (ensures (fun l -> List.length l = pow2 (v63 h)))
       (decreases (v63 h))
let rec to_list_fmtree #h #_ t =
  match t with
  | L c -> [c]
  | N l r ->
    let ll = to_list_fmtree l in
    let lr = to_list_fmtree r in
    FStar.List.Tot.Properties.append_length ll lr;
    FStar.Math.Lemmas.pow2_double_sum (List.length ll);
    ll @ lr

// Counts the number of leaves in a Full Merkle Tree.
val count_leaves_fmtree :
  #h:C.v_height ->
  #hash:C.t_H ->
  t:fmtree h hash ->
  Tot nat
      (decreases (v63 h))
let rec count_leaves_fmtree #_ #_ t =
  match t with
  | L _ -> 1
  | N #_ #_ #_ l r -> count_leaves_fmtree l + count_leaves_fmtree r

// Lemma: every Full Merkle Tree is indeed full.
val lem_fmtree_is_full :
  #h:C.v_height ->
  #hash:C.t_H ->
  t:fmtree h hash ->
  Lemma (ensures (count_leaves_fmtree #h #hash t = pow2 (v63 h)))
        (decreases (v63 h))
let rec lem_fmtree_is_full #h #_ t =
  match t with
  | L _ -> ()
  | N #_ #_ #_ l r ->
    lem_fmtree_is_full l;
    lem_fmtree_is_full r

// Build the default subtree of height h, see Tree.fst for more details.
// To prove the correctness of the Node case, we use the [uncommited_spec]
// axiom declared in the Commitments module.
val make_default_subtree :
  h:C.v_height ->
  Tot (fmtree h (C.uncommitted h))
      (decreases (v63 h))
let rec make_default_subtree h =
  match isz63 h with
  | true -> L (C.hash_to_commitment (C.uncommitted h))
  | _ -> C.uncommitted_spec (pred63 h);
        N (make_default_subtree (pred63 h))
          (make_default_subtree (pred63 h))

// Translation from IMTs to full Merkle trees.
val imt_to_fmtree :
  h:C.v_height ->
  t:imt h ->
  Tot (fmtree h (get_root_height t h))
      (decreases (v63 h))
let rec imt_to_fmtree h t =
  match t with
  | Empty -> make_default_subtree h
  | Leaf c -> L c
  | Node ha l r ->
      N (imt_to_fmtree (pred63 h) l)
        (imt_to_fmtree (pred63 h) r)

val replicate : n:nat -> a:'a -> (l:list 'a{List.length l = n})
let rec replicate n a =
  match n with
  | 0 -> []
  | _ -> a :: replicate (n - 1) a

val lem_replicate_distrib :
  #a:eqtype ->
  n1:nat ->
  n2:nat ->
  a:a ->
  Lemma (replicate (n1 + n2) a =
        (replicate n1 a) @ (replicate n2 a))
let rec lem_replicate_distrib #_ n1 n2 a =
  match (n1, n2) with
  | 0,_ ->
    FStar.List.Tot.Properties.append_nil_l (replicate n2 a)
  | _,0 ->
    FStar.List.Tot.Properties.append_l_nil (replicate n1 a)
  | _,_ ->
    lem_replicate_distrib (n1-1) n2 a

val lem_leaves_default :
  h:C.v_height ->
  Lemma (requires True)
  	(ensures (to_list_fmtree (make_default_subtree h) =
                   (replicate (pow2 (v63 h))
	                      (C.hash_to_commitment (C.uncommitted (lit 0))))))
        (decreases (v63 h))
let rec lem_leaves_default h =
  let dt = make_default_subtree h in
  let ldt = to_list_fmtree dt in
  match isz63 h with
  | true -> ()
  | _ ->
    lem_leaves_default (pred63 h);
    FStar.Math.Lemmas.pow2_double_sum (v63 (pred63 h));
    lem_replicate_distrib (pow2 (v63 (pred63 h))) (pow2 (v63 (pred63 h)))
    			  (C.hash_to_commitment (C.uncommitted (lit 0)))


val lem_translation_preserves_leaves :
  h:C.v_height ->
  t:imt h ->
  Lemma (requires (pow2 (v63 h) >= count_leaves t))
  	(ensures (to_list_fmtree (imt_to_fmtree h t) =
         (to_list t) @ (replicate (pow2 (v63 h) - count_leaves t)
	                          (C.hash_to_commitment (C.uncommitted (lit 0))))))
        (decreases (v63 h))
#push-options "--z3rlimit 100"
let rec lem_translation_preserves_leaves h t =
  let lt = to_list t in
  let fmt = imt_to_fmtree h t in
  let lfmt = to_list_fmtree fmt in
  match t with
  | Empty ->
    lem_leaves_default h
  | Leaf _ -> ()
  | Node hash l r ->
    max_leaves (pred63 h) l;
    max_leaves (pred63 h) r;
    if count_leaves t < pow2 ((v63 h)-1)
    then(
      lemma_incremental_empty_right h t;
      lem_leaves_default (pred63 h);
      lem_translation_preserves_leaves (pred63 h) l;
      FStar.List.Tot.Properties.append_l_nil (to_list l);
      FStar.List.Tot.Properties.append_assoc
        (to_list l)
        (replicate (pow2 ((v63 h) - 1) - count_leaves l)
                                  (C.hash_to_commitment (C.uncommitted (lit 0))))
        (replicate (pow2 ((v63 h) - 1))
                   (C.hash_to_commitment (C.uncommitted (lit 0))));
      FStar.Math.Lemmas.pow2_double_sum ((v63 h) - 1);
      lem_replicate_distrib (pow2 ((v63 h) - 1) - count_leaves l)
                            (pow2 ((v63 h) - 1))
                            (C.hash_to_commitment (C.uncommitted (lit 0)))
      )
    else
      lemma_incremental_full_left h t;
      lem_translation_preserves_leaves (pred63 h) l;
      FStar.List.Tot.Properties.append_l_nil  (to_list l);
      lem_translation_preserves_leaves (pred63 h) r;
      FStar.Math.Lemmas.pow2_double_sum ((v63 h) - 1);
      FStar.Math.Lemmas.subtraction_is_distributive
         (pow2 (v63 h)) (pow2 ((v63 h) - 1)) (count_leaves r);
      FStar.List.Tot.Properties.append_assoc (to_list l) (to_list r)
        (replicate (pow2 ((v63 h) - 1) - count_leaves r)
                   (C.hash_to_commitment (C.uncommitted (lit 0))))

#pop-options


// Correctness and security proofs adapted from [1].
(* lookup function takes a path in the tree, true goes left, false goes right *)

let len = List.length

type data = C.t_C
type hash = C.t_H

type path = l:list bool{len l <= v63 C.max_height}

val get_elt: #h:hash -> path:path -> tree:fmtree (lit (len path)) h -> Tot data
             (decreases path)
let rec get_elt #h path tree =
  match path with
    | [] -> L?.c tree
    | bit::path' ->
      if bit then
        get_elt #(N?.h1 tree) path' (N?.left tree)
      else
        get_elt #(N?.h2 tree) path' (N?.right tree)

(*
 * type for the proof stream which is a list of hashes and looked up data
 *)
type proof =
  | Mk_proof: data:data -> pstream:list hash -> proof

val lenp: proof -> Tot nat
let lenp p = len (Mk_proof?.pstream p)

val p_tail: p:proof{lenp p > 0} -> Tot proof
let p_tail p = Mk_proof (Mk_proof?.data p) (Cons?.tl (Mk_proof?.pstream p))

let p_data = Mk_proof?.data

let p_stream = Mk_proof?.pstream

(*
 * verifier takes as input a proof stream for certain lookup path
 * and computes the expected root node hash
 *)
val verifier: path:path -> p:proof{lenp p = len path} -> Tot hash
let rec verifier path p =
  match path with
    | [] -> C.hash_of_commitment (p_data p)

    | bit::path' ->
      match p_stream p with
        | hd::_ ->
          let h' = verifier path' (p_tail p) in
          if bit then
            C.merkle_hash (lit (len path - 1)) h' hd
          else
            C.merkle_hash (lit (len path - 1)) hd h'

(*
 * prover function, generates a proof stream for a path
 *)
val prover: #h:hash ->
            path:path ->
            tree:fmtree (lit (len path)) h ->
            Tot (p:proof{lenp p = len path})
            (decreases path)
let rec prover #h path tree =
  match path with
    | [] -> Mk_proof (L?.c tree) []

    | bit::path' ->
      let N #dc #hl #hr left right  = tree in
      if bit then
        let p = prover path' left in
        Mk_proof (p_data p) (hr::(p_stream p))
      else
        let p = prover path' right in
        Mk_proof (p_data p) (hl::(p_stream p))

(*
 * correctness theorem: honest prover's proof stream is accepted by the verifier
 *)
val correctness: #h:hash ->
                 path:path ->
                 tree:fmtree (lit (len path)) h ->
                 p:proof{p = prover path tree} ->
                 Lemma (requires True) (ensures (verifier path p = h))
                 (decreases path)
let rec correctness #h path tree p =
  match path with
    | [] -> ()
    | bit::path' ->
      if bit then
        correctness #(N?.h1 tree) path' (N?.left tree) (p_tail p)
      else
        correctness #(N?.h2 tree) path' (N?.right tree) (p_tail p)



(*
 * security theorem: the only way a verifier can be tricked into accepting a proof
 * stream for an non existent element is if there is a hash collision.
 * As the merkle_hash is assumed perfect in Commitments, we can prove
 * this will never happen.
 * In other words, if a proof is verified, then its data must be correct.
 *)
val security: #h:hash ->
              path:path ->
              tree:fmtree (lit (len path)) h ->
              p:proof{lenp p = len path /\ verifier path p = h} ->
              Lemma (ensures (get_elt path tree = p_data p))
                    (decreases path)

let rec security #h path tree p =
  match path with
  | [] ->
    assert (L?.c tree = C.hash_to_commitment h);
    ()
  | bit::path' ->
    let N #dc #h1 #h2 left right = tree in
    let h' = verifier path' (p_tail p) in
    let hd = Cons?.hd (p_stream p) in
    if bit then
      security path' left (p_tail p)
    else
      security path' right (p_tail p)

// Corollary, correctness also holds on the translation [imt_to_fmtree].
val cor_correctness_translation :
  path:path ->
  tree:imt (lit (len path)) ->
  p:proof{p = prover path (imt_to_fmtree (lit (len path)) tree)} ->
  Lemma (requires True)
        (ensures (verifier path p = (get_root_height tree (lit (len path)))))
let cor_correctness_translation path tree p =
  correctness path (imt_to_fmtree (lit (len path)) tree) p

// Corollary, security also holds on the translation [imt_to_fmtree].
val cor_security_translation :
  path:path ->
  tree:imt (lit (len path)) ->
  p:proof{lenp p = len path /\
          verifier path p = (get_root_height tree (lit (len path)))} ->
  Lemma (ensures (get_elt path (imt_to_fmtree (lit (len path)) tree) = p_data p))
let cor_security_translation path tree p =
  security path (imt_to_fmtree (lit (len path)) tree) p


// Additional Merkle property (merkle_framing):
// forall h1, h2, t1, t2, v1, v2, pos, proof.
//     verify (h1, v1, pos, proof)
//  /\ verify (h2, v2, pos, proof)
//  /\ h1 = root t1  /\ h2 = root t2
//  ==>
//  (forall pos'. pos' <> pos ==> get pos' t1 = get pos0 t2)


// First we prove the following lemma:
// If two trees have the same root, then they are equal.
// This follows from the hash injectivity assumption.
// We need to assume that the [hash_of_commitment] function is also injective,
// which is reasonable given the previously mentioned assumption.
val lem_roots_injective :
  #h1:hash ->
  #h2:hash ->
  n:C.v_height ->
  t1:fmtree n h1 ->
  t2:fmtree n h2 ->
  Lemma (requires (h1 = h2))
        (ensures (t1 = t2))
        (decreases (v63 n))
let rec lem_roots_injective #h1 #h2 n t1 t2 =
  match t1 with
  | L __ ->
    assume(forall c1 c2. C.hash_of_commitment c1 = C.hash_of_commitment c2 ==> c1 = c2)
  | N #n1 #hl1 #hr1 l1 r1 ->
    let N #n2 #hl2 #hr2 l2 r2 = t2 in
    lem_roots_injective n1 l1 l2;
    lem_roots_injective n1 r1 r2

val merkle_framing :
  #h1:hash ->
  #h2:hash ->
  path:path ->
  tree1:fmtree (lit (len path)) h1 ->
  tree2:fmtree (lit (len path)) h2 ->
  p1:proof{lenp p1 = len path /\ verifier path p1 = h1} ->
  p2:proof{lenp p2 = len path /\ p_stream p2 = p_stream p1 /\
           verifier path p2 = h2} ->
  Lemma (ensures (forall path'. len path' = len path /\ path' <> path ==>
                           get_elt path' tree1 = get_elt path' tree2))
        (decreases path)
let rec merkle_framing #h1 #h2 path tree1 tree2 p1 p2 =
  match path with
  | [] -> ()
  | bit::path' ->
    let N #_ #hl1 #hr1 l1 r1 = tree1 in
    let N #_ #hl2 #hr2 l2 r2 = tree2 in
    let s::_ = p_stream p1 in
    if bit then (
      lem_roots_injective #hr1 #hr2 (pred63 (lit (len path))) r1 r2;
      merkle_framing #hl1 #hl2 path' l1 l2 (p_tail p1) (p_tail p2))
    else (
      lem_roots_injective #hl1 #hl2 (pred63 (lit (len path))) l1 l2;
      merkle_framing #hr1 #hr2 path' r1 r2 (p_tail p1) (p_tail p2))
