ifndef FSTAR_HOME
   FSTAR_HOME=$(dir $(shell which fstar.exe))..
endif

include $(FSTAR_HOME)/lib/fstar/ml/Makefile.include

FSTAR_FILES ?= Commitments.fsti Int63.fsti Tree.Data.fst Tree.Properties.fst Tree.Methods.fst Tree.InsertionIdentities.fst Tree.fst FullMTree.fst Tree.PrePost.fst

CACHE_DIR ?= _cache

OUT_DIR ?= _out

HINTS_DIR ?= _hints

FSTAR_FLAGS = \
	--cache_checked_modules \
	--use_hints \
	--hint_dir $(HINTS_DIR) \
	--record_hints \
	--already_cached 'Prims FStar' \
	--cache_dir $(CACHE_DIR)

FSTAR = $(FSTAR_HOME)/bin/fstar.exe $(FSTAR_FLAGS)

SPEC_EXTRACTION_MODULES = SpecExtraction.Util SpecExtraction.PrePost SpecExtraction.TestGeneration SpecExtraction

OCAML_PACKAGES = \
	digestif.c tezos-hacl-glue-unix tezos-sapling tezos-stdlib \
	tezos-protocol-alpha tezos-alpha-test-helpers tezos-error-monad \
	tezos-lwt-result-stdlib base qcheck stdint zarith

OCAML = ocamlfind opt -linkpkg -g -thread -w -8 -I ml \
	$(addprefix -package , $(OCAML_PACKAGES))

all: setup verify-all

setup:
	opam switch create sapling-verification ocaml-base-compiler.4.12.0
	opam install ocamlfind batteries pprint stdint yojson zarith ppxlib -y
	# opam repo add custom git+https://github.com/antoniolocascio/opam-repository
	opam install tezos-alpha-test-helpers digestif tezos-sapling tezos-protocol-alpha tezos-test-helpers qcheck -y

verify-all:
	$(FSTAR) $(FSTAR_FILES)

clean:
	rm -rf $(CACHE_DIR) .depend
	rm -rf $(OUT_DIR)

extract:
	$(FSTAR) $(FSTAR_DEFAULT_ARGS) --codegen OCaml --odir $(OUT_DIR) \
		$(addprefix --extract , $(SPEC_EXTRACTION_MODULES)) \
		$(addsuffix .fst, $(SPEC_EXTRACTION_MODULES))
	env OCAMLPATH=$(FSTAR_HOME)/bin/ ocamlfind ocamlopt -w -8 -shared -I $(OUT_DIR) \
		-package fstar-tactics-lib -o $(OUT_DIR)/SpecExtraction.cmxs \
		$(addprefix $(OUT_DIR)/, $(addsuffix .ml, $(subst .,_,$(SPEC_EXTRACTION_MODULES))))
	$(FSTAR) $(FSTAR_DEFAULT_ARGS) --codegen OCaml --include $(OUT_DIR) \
		--load SpecExtraction --no_plugins --odir $(OUT_DIR) \
		--print_full_names --print_implicits \
		--extract "-Tree +Tree.Data +Tree.Properties +Tree.Methods +Tree.InsertionIdentities +Tree.PrePost" \
		$(addsuffix .fst, $(SPEC_EXTRACTION_MODULES)) $(FSTAR_FILES)
	./cleanup

build-main:
		$(OCAML) -I $(OUT_DIR) ml/prims.ml ml/Util.ml ml/Int63.ml ml/Commitments.ml \
		$(OUT_DIR)/Tree_Data.ml $(OUT_DIR)/Tree_Methods.ml \
		$(OUT_DIR)/Tree_Properties.ml $(OUT_DIR)/Tree_InsertionIdentities.ml \
		ml/Tree.ml ml/main.ml -o $(OUT_DIR)/main.exe

verify:
	$(FSTAR) $(FSTAR_DEFAULT_ARGS) --codegen OCaml --extract Tree --odir $(OUT_DIR) \
		Tree.fst Commitments.fsti

test-proto:
	$(OCAML) -I $(OUT_DIR) ml/prims.ml ml/Util.ml ml/Int63.ml ml/Commitments.ml ml/FStar_QCheck.ml $(OUT_DIR)/Tree_Data.ml \
		$(OUT_DIR)/Tree_Methods.ml $(OUT_DIR)/Tree_Properties.ml $(OUT_DIR)/Tree_InsertionIdentities.ml \
		ml/Tree.ml \
		ml/test/proto_test.ml -o $(OUT_DIR)/test-proto.exe

test:
		$(OCAML) -I $(OUT_DIR) -I ml/test ml/prims.ml ml/Util.ml ml/Int63.ml ml/Commitments.ml ml/FStar_QCheck.ml $(OUT_DIR)/Tree_Data.ml \
		$(OUT_DIR)/Tree_Methods.ml $(OUT_DIR)/Tree_Properties.ml $(OUT_DIR)/Tree_InsertionIdentities.ml \
		ml/Tree.ml \
		ml/test/generators.ml ml/test/test_internal_tree.ml \
		-o $(OUT_DIR)/test.exe

.PHONY: all clean
