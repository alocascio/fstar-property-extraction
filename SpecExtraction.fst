module SpecExtraction

open FStar.InteractiveHelpers.ExploreTerm
open FStar.Tactics
open FStar.InteractiveHelpers.Base

open SpecExtraction.Util
open SpecExtraction.PrePost
open SpecExtraction.TestGeneration

let get_type (id:string) : Tac typ =
  let get_sigelt_typ (r:sigelt) : Tac typ =
    match inspect_sigelt r with
    | Sg_Val _ us typ -> typ
    | Sg_Let _ lbs -> begin
      let lbv = lookup_lb_view lbs (explode_qn id) in
      lbv.lb_typ
      end
    | _ -> fail "Not a val or let in toplevel"
  in
  // lookup the type of id among the toplevel declarations
  let r: option sigelt = lookup_typ (top_env ()) (explode_qn id) in
  guard (Some? r);
  let Some r = r in
  let typ = get_sigelt_typ r in
  // We first resolve the type synonyms
  let typ = resolve_type_synonyms typ in
  // We then normalize the resulting type to make the exploration simpler
  let typ = norm_term_env (top_env ()) [hnf] typ in
  // We remove any remaining calls to b2t.
  let typ = visit_tm remove_b2t typ in
  typ


let extract_spec (fn : string) : Tac decls =
  let typ = get_type fn in
  let (term_pre, typ_pre, term_post, typ_post, isL) = pre_post typ in
  let term_pre = remove_and_true term_pre in
  match explode_qn fn with
  | Cons x xs ->
    let fmodule = implode_qn (List.Tot.Base.init (Cons x xs)) in
    let smodule = moduleof (top_env ()) in
    let fname = List.Tot.Base.last (Cons x xs) in
    let lb_pre = pack_lb ({lb_fv = pack_fv (smodule@[fname ^ "_pre"]);
                           lb_us = [];
                           lb_typ = typ_pre;
                           lb_def = term_pre}) in
    let sv_pre : sigelt_view = Sg_Let false [lb_pre] in
    let lb_post = pack_lb ({lb_fv = pack_fv (smodule@[fname ^ "_post"]);
                            lb_us = [];
                            lb_typ = typ_post;
                            lb_def = term_post}) in
    let sv_post : sigelt_view = Sg_Let false [lb_post] in
    let ses : list sigelt = [pack_sigelt sv_pre; pack_sigelt sv_post] in
    // Make the QCheck tests. For now, we instantiate every type variable
    // with int.
    let ses' = make_test fname fmodule smodule typ isL (`Int63.t) in
    (ses @ ses')
  | _ -> fail "Not an id"
