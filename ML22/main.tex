\documentclass[sigplan,screen,review]{acmart}
\settopmatter{printfolios=true,printccs=false,printacmref=false}
\setcopyright{none}
%\setcopyright{acmcopyright}
%\setcopyright{acmlicensed}
%\setcopyright{rightsretained}
%\copyrightyear{2022}           %% If different from \acmYear

%% Language and font encodings
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}


% \usepackage{amsmath}
% \usepackage{amssymb}
\usepackage{natbib}
\bibliographystyle{acm}
%\bibliographystyle{dinat}
%\bibliographystyle{plain}
\usepackage{combelow}

\usepackage{xspace}
\usepackage{minted}
\usepackage{enumerate}

%Erase FancyFoot/Header, copyright, references to PACMPL

\makeatletter
\def\@copyrightspace{\relax}
\makeatother

\renewcommand{\acmArticle}[1]{\relax}
\renewcommand{\footnotetextcopyrightpermission}[1]{\relax}

%% Conference information
%% Supplied to authors by publisher for camera-ready submission;
%% use defaults for review submission.
%\acmConference[]{}{}{}
%\acmYear{}
%\acmISBN{} % \acmISBN{978-x-xxxx-xxxx-x/YY/MM} \acmDOI{} %
%\acmDOI{}
%\startPage{1}

\newcommand{\fst}{F$^{\star}$\xspace}

\title{Verify, but test}
\subtitle{Extracting property-based tests from \fst specifications}
\author{Antonio Locascio}
\author{Germ\'{a}n Andr\'{e}s Delbianco}
\author{Marco Stronati}
\email{{antonio.locascio, german, marco.stronati}@nomadic-labs.com}
\affiliation{%
  \institution{Nomadic Labs}
  \city{Paris}
  \country{France}
}

\begin{document}

% Hack acmart fancy headers
\makeatletter
\fancyhead[L]{\@headfootfont\shorttitle}%
\fancyhead[R]{\@headfootfont\@shortauthors}%
\makeatother


\begin{abstract}
The formal, mechanized, verification of computer programs is a
particularly difficult task in fast-evolving and complex development
environments, as it is typically a slow process. To speed this effort
up, verification efforts often target abstract models of these
programs, and many implementation details are often left unverified in
critical code. In this work, we propose to bridge the verification gap
between a mechanized model and its implementation by leveraging
property-based testing. Concretely, we present a light-weight
automated tool for extracting \fst specifications as OCaml predicates
for its execution as a QCheck tests.
\end{abstract}

\maketitle

\section{Introduction}

A typical workflow when developing high-assurance software is to start
from a prototype to better understand the problem at hand, write a
specification for the desired system (having learnt from all the
mistakes in the process of implementing said prototype), write a
reference implementation of the specification, and then formally
verify that the implementation matches the specification, a process
that historically is slow and costly.

However in a fast paced development cycle, the not-yet-verified
implementation is often sent to production, continues to be improved
over time and becomes a moving target for the verification effort,
which often does not bear fruit. This is not a new problem. One of the
main goals of formal verification tools has been to reduce the cost of
verification, to allow it to be better integrated in the development
cycle (see for example proof-oriented programming\cite{fstar-web}).
% For example, in many cases it is possible today
% to extract an implementation that matches a specification, thus merging
% the last two steps of the workflow described above.

Going a step further, we would like to start sketching a specification
while we develop the prototype and use this model to test the
prototype itself. Thus moving from a linear development process to an
iterative one, where implementation and specification are constantly
refined and adapted. While abstracting away implementation details in
the specification, the \emph{verification gap} problem arises: given
that the verified model is loosely coupled with the implementation, do
the properties that we have proved for the model really hold for the
implementation?

One way of addressing the \emph{verification gap} problem is to rely
on Property-Based Testing (PBT)~\cite{pbt}, that is, to extract the
model as properties that can be executed as tests on the
implementation.
%
The focus of this work is on a lightweight automated tool for
extracting \fst specifications to OCaml, and its execution as a QCheck
test against an OCaml implementation.

We propose to OCaml programmers to write specifications as \fst
programs, instead of manually writing QCheck tests.
%
By doing this, our tool will automatically extract the specification
as QCheck properties, which can then be used to test either the code
extracted from the model, or another implementation.

Thanks to this approach, rather than waiting for a finished
implementation and work on a complete formal verification, we can move
along a \emph{verification spectrum}. Start with a prototype
implementation tested by PBT on its co-developed abstract model,
refine the model over time in order to extract parts of it and replace
them in the implementation until (possibly) a full extraction can be
achieved.

It should be noted that a full extraction is sometimes too costly, as
it may require to precisely represent in the model complex parts of
the code that are not essential to the program's core functionality.
However, being able to find the sweet spot along this spectrum where
only the crucial parts have been verified is still very valuable and
common practice.

It's not required to do a full verification of the code in order to
use our tool. One could specify an abstract model of the
implementation, whose properties could be tested against the more
complex OCaml implementation by using the former as a reference
implementation. Further, one might even write the specification as an
axiomatization of the implementation (i.e, \emph{assuming} the
properties), and leave their actual proofs as a future step.

%% Even in the cases where a full extraction can be achieved, the
%% complex
% toolchains and large trusted computing base can still benefit from a
% testing infrastructure.
% For example it is common practice to assume (and
% axiomatize) the existence of a \texttt{List} library that, during
% extraction, will be linked to the \texttt{List} module of the standard
% library, despite the fact that there is no formal connection between the
% two.
% For this reason, even if our properties have already been proved in
% the model, they should also be tested in the extracted implementation.
% In other words the \emph{verification gap} may never be really closed in
% full, even at the end of the spectrum, and property based testing is a
% way to account for it along the development process.

% The rest of the document is structured as follows. We first give a brief
% overview of the F* language and Property-Based Testing. Then, we give a
% detailed account of our proposed specification extraction tool for F*.
% After that, we present the case study for our workflow, Incremental
% Merkle Trees, which illustrates the wide spectrum of verification and
% testing strategies. Next, we describe our experience of working with F*,
% and explain our contributions to that project. Finally, we discuss
% related projects and some future work.

\section{Overview}

\subsection{Position}
This work was carried out in the context of the
Tezos\cite{Goodman2014TezosA,AllombertBT19} blockchain.
%
Octez\cite{tezos-web}, its OCaml implementation, is a fast-evolving
codebase: new versions of the Tezos protocol are consistently
activated and made available to users roughly every 3 months.
%
It is also a large codebase: the core Tezos protocol implementation
extends over ~150 Kloc, and the whole codebase (counting the
implementation of several Tezos protocols, testing suites, and other
dedicated in-house libraries) comprises roughly 1Mloc of OCaml code.
%
Moreover, it relies upon many advanced OCaml features: monadic
programming encoding different layers of effects, GADTs, and
first-class functors.
%
% Due to its critical nature, several verification efforts have been
%on different parts of this implementation.  Cite some tezos verif
%works

In particular, our proposed workflow of verification and testing was
applied to the implementation of the \textit{Incremental Merkle Tree}
\cite{sapling-tezos} data structure, used in the \textit{Sapling
  protocol}\cite{zcash-spec} sub-component of the Tezos protocol.
%
This was an ideal target for this project, as:

\begin{itemize}
\item It's a self-contained piece of code, which only depends on
  well-specified cryptographic primitives.
\item The data structure has \emph{clear} and simple invariants, yet
  establishing that they are preserved by the implementation is not
  trivial.
\item There are multiple implementations for it, of varying
  complexity, which help showcase the wide spectrum of verification
  and testing models.
\end{itemize}

\paragraph{The \fst language}

\fst~\cite{fstar-web,SwamyCFSBY11} is an ML-style functional
programming language aimed at formal verification. \fst combines
automated verification through an SMT solver with the interactive
functionality of a proof assistant based on dependent types, making it
a \emph{hybrid} verification tool. After the verification step, an
\fst program can be extracted to OCaml in order to be
executed. Additionally, \fst supports
meta-programming~\cite{metafstar}, which is a crucial feature for this
work as it allows the extraction of specifications.


\paragraph{Property-based testing with QCheck}

Property-based testing\cite{pbt} (or PBT for short) is a testing
discipline in which properties about programs are checked to be true
by validating them on a large number of randomly generated
examples.
%
Property-based testing a program generally requires the user to do two
things: (i) to define the desired properties about the code, and (ii)
to provide functions for generating random inputs for those properties
(commonly known as \textit{generators}).

To aid the definition of these properties and custom data type
generators, several libraries have been developed.
%
One of these is QCheck\cite{qcheck}, which offers a wide range of
combinators to property-test OCaml programs.
%
In this project we present a tool\cite{tool} that automatically
extracts QCheck properties from \fst specifications. The generator
definition, however, is still left to the user.

\subsection{From \fst specs to QCheck tests}

In order to extract a specification written in \fst as properties for
property-based testing we introduce a simple Meta-\fst\cite{metafstar}
tactic called \mintinline{fstar}{make_test}.

This tactic does two things. First, it extracts a function's pre and
post-conditions as OCaml Boolean functions. Second, it synthesizes the
QCheck boilerplate code necessary for defining the test.

The main functionality consists of extracting a function's or a
lemma's pre and post-conditions as OCaml Boolean predicates. For
instance, given the following \fst function:

\begin{minted}{fstar}
val foo :
  x:T{P1 x} ->
  Pure T' (requires (P2 x))
          (ensures (fun y -> Q x y))
let foo x = ...
\end{minted}

This signature tells us that \texttt{foo} is a pure function that
expects an argument \texttt{x} of type \texttt{T} such that \texttt{P1
  x} holds, and returns a value of type \texttt{T'}. Additionally, the
\texttt{requires} and \texttt{ensures} clauses assert that \texttt{P2
  x} must also hold, and that the post-condition \texttt{Q x y} must
be satisfied, where \texttt{y} is the value returned by this
function. We will call properties enforced in type refinements
\textit{implicit} pre/post-conditions, while the ones found in the
\texttt{requires} and \texttt{ensures} clauses will be referred to as
\textit{explicit}.

One can call the \mintinline{fstar}{extract_spec} tactic through a
\texttt{splice}, which inserts syntax generated through
Meta-\fst. This is done by adding the following line (anywhere
\texttt{foo} is in scope):

\begin{minted}{fstar}
%splice[] (extract_spec (`%foo))
\end{minted}

As a result, in the extracted OCaml code of the module where the
splice was called, the following predicates will be defined:

\begin{minted}{ocaml}
let foo_pre = fun x -> P1 x && P2 x
let foo_post = fun x -> fun y -> Q x y
\end{minted}

As shown in this example, a function's pre-condition is parametrized
by the function's arguments, while its post-condition takes as an
additional argument the function's return value.

The second step, generating the QCheck tests, is performed by the
\mintinline{fstar}{make_test} tactic (called by
\mintinline{fstar}{extract_spec}).  Continuing with the example, this
tactic generates the following declarations:

\begin{minted}{ocaml}
let (gen_foo_args : T FStar_QCheck.arbitrary) =
  FStar_QCheck.undefined_gen
    "Not yet implemented"

let (test_foo_spec : FStar_QCheck.test_t) =
  FStar_QCheck.test_make "foo_spec" gen_foo_args
    (fun a0 ->
      match a0 with
      | x -> (FStar_QCheck.assume_ (foo_pre x);
              foo_post x (foo x)))
\end{minted}

The first of these, \mintinline{ocaml}{gen_foo_args} corresponds to a
template definition for the QCheck generator for \texttt{foo}'s
arguments. Deriving the generators is outside the scope of our tool,
and this stub is provided for the user to complete.

The second one, \mintinline{ocaml}{gen_foo_args}, is the definition of
the QCheck test, in which the extracted pre-condition is assumed and
the post-condition is asserted. The \texttt{FStar\_QCheck} module is
just a simple interface wrapper to expose necessary \texttt{QCheck}
functions in \fst.

\subsection{Tool design: extraction via meta-programming}

The design of our spec extraction tool revolves around Meta-\fst
tactics. Meta-\fst~\cite{metafstar} is a meta-programming framework
for \fst, i.e. a library for manipulating \fst terms from within
\fst. A \textit{tactic}, then, is simply a function that uses
Meta-\fst.

The bulk of the specification extraction work is performed by the
\texttt{extract\_spec} tactic, which takes as argument the name of the
function the user want to extract a specification from. We'll give an
overlook of the algorithm it implements:

\begin{enumerate}
\item The first step is to query the environment to get the function's
  type. This could either be annotated in a signature or inferred.

\item The retrieved type is subject to some (mostly trivial)
  transformations. The most interesting pre-processing step is
  \textit{synonym resolution} (for example, turning \texttt{nat} into
  \texttt{v:int\{v >= 0\}}), which is crucial to capture implicit
  invariants hidden in refinements.

\item Next, the type is traversed, one arrow at a time, collecting the
  names, types, and refinements for each argument. For each refinement
  we abstract all the previous binders. For now, we leave the final
  computation type \texttt{C} untouched. It's important to note that
  an argument's type might have nested refinements, e.g.
\[
\texttt{x:(v:int\{v >= 0\})\{x < 5\}}
\]
In this step, these refinements are flattened to get
\[
\texttt{fun x -> x >= 0 \&\& x < 5}
\]
\item All of the arguments' refinements are then joined to get the
  function's implicit pre-condition.

\item Finally, the final computation type \texttt{C} is
  inspected. From it the post-condition (both from an ensures clause
  and the return type's refinement) and potential explicit
  pre-condition (requires clause) are retrieved.  When there is an
  explicit pre-condition, it is joined with the computed implicit
  pre-condition.
\end{enumerate}

\section{Discussion and Related work}

\paragraph{Extraction vs Lightweight Validation}
%
Another well-established workflow is to directly extract correct
implementations automatically from mechanized implementations.
%
Projects like the Verified Software Toolchain\cite{vst,Appel11} narrow
the verification gap by building a vertical stack of mechanized
components, building on the foundations provided by the Compcert
certified C compiler\cite{compcert,Leroy06}.
%
Closer to \fst, the KaRaMeL\cite{karamel} project allows the
extraction of certified programs written in Low$^{\star}$\xspace, a
subset of \fst, to \texttt{C}.

Even when this approach has been successfully adopted in large
industrial contexts, it is hard to apply to a codebase like Tezos',
which is naturally designed to evolve.
%
Instead, we propose to tackle the verification gap by leveraging the
automated extraction of property-based tests from a formal
specification in order to validate that the implementation complies
with their formal model.

\paragraph{QuickChick}
%
QuickChick\cite{quickchick} is a randomized property-based testing
plugin for the Coq Proof Assistant. Its central idea is
\textit{foundational testing}, which means that the testing code is
formally verified to be testing the desired property. Additionally,
QuickChick supports automatic derivation of generators for data
satisfying a particular predicate.
%
Our approach is more light-weight: implementing QuickChick's features
for \fst, would have required a very significant effort, and QCheck
was already integrated into the Octez test suite.

There has also been some recent work\cite{pbt-coq-ocaml} leveraging
QuickChick for testing OCaml code. Their workflow is the opposite of
ours: they use \texttt{coq-of-ocaml}\cite{coq-of-ocaml} to translate
OCaml definitions into Coq, and then rely on QuickChick to test the
implementation.
%
This approach suffers from the limitation that
\texttt{coq-of-ocaml} cannot fully translate all OCaml features into
Gallina (Coq) without making arbitrary design decisions which carry
semantic weight.

\paragraph{Monolith} Monolith\cite{monolith,Pottier21}
is a framework for testing OCaml libraries that supports random
testing and fuzzing. The user has to specify the library's interface
(types and operations) and provide a reference implementation. Then,
Monolith runs sequences of operations trying to find unexpected
behaviours.
%
Given that we propose extracting reference implementations from
formally verified abstract models, it would be interesting to study if
those could be integrated with Monolith. This way, the open issue of
defining generators could be solved by making use of Monolith's
fuzzing and random testing features.

\paragraph{Extracting effectful specifications}
%
Currently, our tool targets only \textit{pure} \fst programs with
Boolean pre and post-conditions. An interesting line of future work
would be to extend it to support other \fst monadic effects, and then
leverage the specification extraction mechanism to test arbitrary
monadic OCaml code implementing such effects. For example, in our
setting this could be used for directly extracting a specification for
the more complex stateful implementation of Incremental Merkle Trees.

\bibliography{refs.bib}

\end{document}
