(* Mostly wrappers to avoid using F* lib *)
(* Used by the cleanup script, so it should disappear in the future *)

let (<<) f g x = f (g x)

let fix_snd f g x y = f x (g y)

