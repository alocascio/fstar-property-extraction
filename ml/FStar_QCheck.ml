(* QCheck wrapper for F* *)

type 'a arbitrary = 'a QCheck.arbitrary

let assume_ = QCheck.assume

type test_t = QCheck.Test.t

let test_make (nm:string) a p =
  QCheck.Test.make ~name:nm ~count:100 a p

let undefined_gen (str:string) =
  failwith str
