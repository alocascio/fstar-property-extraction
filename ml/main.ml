open Tezos_sapling

(* Arbitrary hash generator *)
let rand_hash _ = Core.Validator.Hash.of_bytes_exn (Bytes.create 32)

let c1 = Core.Validator.Hash.to_commitment (rand_hash ())
let c2 = Core.Validator.Hash.to_commitment (rand_hash ())
let c3 = Core.Validator.Hash.to_commitment (rand_hash ())
let c4 = Core.Validator.Hash.to_commitment (rand_hash ())

(* Build a tree using the internal API *)
let empty_t = Tree.Empty
let t1 = Tree_InsertionIdentities.insert_model c1 32 empty_t
let t2 = Tree_InsertionIdentities.insert_pow c2 (Stdint.Uint32.of_int 1) 32 t1
let t4 = Tree.insert_list [c3;c4] (Stdint.Uint32.of_int 2) 32 t2

(* Build the same tree, but through the high level interface *)
let empty = Tree.empty
let t1' = Tree.add c1 empty
let t2' = Tree.add c2 t1'
let t4' = Tree.add_list [c3;c4] t2'

let _ = assert(snd t4' = t4)

let print_z n = print_int (Z.to_int n)

let _ = print_z (Tree_Properties.count_leaves t4)
