module L = List
open Tezos_protocol_alpha
open Tezos_alpha_test_helpers
open Tezos_error_monad
open Error_monad
open Tezos_lwt_result_stdlib
open Protocol
open Lwtreslib.Bare
open Sapling_helpers.Common
open Tezos_stdlib.Utils.Infix
module R = Tezos_raw_protocol_alpha
module H = Tezos_sapling.Core.Client.Hash

open Tree_Data
open Tree_Methods
open Tree_Properties

module LM = Landmark_threads

open Tezos_sapling
module Core = Core.Client
module Storage = Storage.Make_Storage (Core)

let initial_size = 100000
let to_insert = 500

let left node = Int64.mul node 2L

let right node = Int64.(add (mul node 2L) 1L)

let wrap_tr m =
  m >|= function
  | Ok x -> Ok x
  | Error e -> Error (Environment.wrap_tztrace e)

let tag_make_context = LM.register "make_context"
let tag_test_a = LM.register "test_a"
let tag_test_a_root = LM.register "test_a_root"
let tag_test_b = LM.register "test_b"
let tag_proj0 = LM.register "proj0"
let tag_proj1 = LM.register "proj1"
let tag_insert = LM.register "insert"
let tag_insert' = LM.register "insert'"
let tag_rand_cms = LM.register "rand_cms"
let tag_eq_t = LM.register "eq_t"
let tag_pre = LM.register "pre"
let tag_post = LM.register "post"

(* Arbitrary hash generator *)
let rand_hash _ =
  Core.Hash.of_bytes_exn (Bytes.create 32)
let rand_commitment _ =
  Core.Hash.to_commitment (rand_hash ())

let rand_cms n =
  LM.enter tag_rand_cms;
  let l = L.init n (fun _ -> rand_commitment ()) in
  LM.exit tag_rand_cms;
  l

let rec project_tree c i node h =
  R.Storage.Sapling.Commitments.find (c, i) node
  >>=? (function
  | (ctx, None) -> return Empty
  | (_, Some hash) ->
    if h = 0
    then return (Leaf (H.to_commitment hash))
    else (
      project_tree c i (left node) (h - 1)
      >>=? fun l ->
      project_tree c i (right node) (h - 1)
      >>=? fun r ->
      return (Node (hash, l, r))
    ))

let make_context n =
  let rec add_safe ctx id n pos =
    if n <= 1000
    then
      (let cms = rand_cms n in
      Sapling_storage.Commitments.add ctx
                                      id
                                      cms
                                      (Int64.of_int pos)
      >>= wrap
      >>=? fun (ctx, _size) ->
      return (ctx, id))
    else
      (let cms = rand_cms 1000 in
      Sapling_storage.Commitments.add ctx
                                      id
                                      cms
                                      (Int64.of_int pos)
      >>= wrap
      >>=? fun (ctx, _size) ->
      add_safe ctx id (n - 1000) (Int.add pos 1000)
      )
  in
  LM.enter tag_make_context;
  let m = (
    Context.init 1
    >>=? fun (b, _) ->
    Raw_context.prepare
      b.context
      ~level:b.header.shell.level
      ~predecessor_timestamp:b.header.shell.timestamp
      ~timestamp:b.header.shell.timestamp
      ~fitness:b.header.shell.fitness
    >>= wrap
    >>=? fun ctx ->
    Lazy_storage_diff.fresh
      Lazy_storage_kind.Sapling_state
      ~temporary:false
      ctx
    >>= wrap
    >>=? fun (ctx, id) ->
    Sapling_storage.init ctx id ~memo_size:0
    >>= wrap
    >>=? fun ctx ->
    add_safe ctx id initial_size 0
    >>=? fun (ctx, id) ->
    return (ctx, id) )
  in
  LM.exit tag_make_context;
  m

(* insert' . proj = proj . insert *)
let test_a ctx id n =
  LM.enter tag_test_a;
  let m = (
    wrap_tr (LM.enter tag_proj0;
             let m = project_tree ctx id 1L 32 in
             LM.exit tag_proj0;
             m) >>=? fun t0' ->
    let cms = rand_cms to_insert in
    (LM.enter tag_insert;
    let m = Sapling_storage.Commitments.add
              ctx id cms (Int64.of_int n) in
    LM.exit tag_insert;
    m)
    >>= wrap >>=? fun (ctx, _size) ->
    wrap_tr (LM.enter tag_proj1;
             let m = project_tree ctx id 1L 32 in
             LM.exit tag_proj1;
             m) >>=? fun t1' ->
    let t1'' = (LM.enter tag_insert';
               let t = insert_list cms (Stdint.Uint32.of_int n) 32 t0' in
               LM.exit tag_insert';
               t) in
    let b = (LM.enter tag_eq_t;
             let b = t1' = t1'' in
             LM.exit tag_eq_t;
             b) in
    return b )
  in
  LM.exit tag_test_a;
  m

let test_a_root ctx id n =
  LM.enter tag_test_a_root;
  let m = (
    wrap_tr (LM.enter tag_proj0;
              let m = project_tree ctx id 1L 32 in
              LM.exit tag_proj0;
              m) >>=? fun t0' ->
    let cms = rand_cms to_insert in
    (LM.enter tag_insert;
    let m = Sapling_storage.Commitments.add
              ctx id cms (Int64.of_int n) in
    LM.exit tag_insert;
    m)
    >>= wrap >>=? fun (ctx, _size) ->
    Sapling_storage.Commitments.get_root ctx id
    >>= wrap
    >>=? fun (ctx, root_proto) ->
    let t1'' = (LM.enter tag_insert';
                let t = insert_list cms (Stdint.Uint32.of_int n) 32 t0' in
                LM.exit tag_insert';
                t) in
    let root_adt = get_root_height t1'' 32 in
    let b = (let b = root_adt = root_proto in
              b) in
    return b )
  in
  LM.exit tag_test_a_root;
  m

let (insert_list_pre :
  Commitments.t_C list ->
    Stdint.Uint32.t ->
      Int63.t ->
        (Commitments.t_C, Commitments.t_H) Tree_Data.tree -> bool)
  =
  fun vs ->
    fun pos ->
      fun h ->
        fun t ->
          (((Int63.gte h (0)) &&
              (Int63.lte h Commitments.max_height))
              &&
              ((Tree_Properties.incremental h t) &&
                (Tree_Properties.merkle h t)))
            &&
            Z.((Z.leq (Z.of_int (L.length vs))
                (Z.sub (Z.pow (Z.of_int 2) h) (Z.of_int (Stdint.Uint32.to_int pos))))
                && ((Z.of_int (Stdint.Uint32.to_int pos)) = (Tree_Properties.count_leaves t)))
let (insert_list_post :
  Commitments.t_C list ->
    Stdint.Uint32.t ->
      Int63.t ->
        (Commitments.t_C, Commitments.t_H) Tree_Data.tree ->
          (Commitments.t_C, Commitments.t_H) Tree_Data.tree -> bool)
  =
  fun vs ->
    fun pos ->
      fun h ->
        fun t ->
          fun t' ->
            ((Tree_Properties.incremental h t') &&
               (Tree_Properties.merkle h t'))
              &&
              ((Tree_Methods.to_list t') =
                 (L.append (Tree_Methods.to_list t) vs))

(* pre ==> post *)
let test_b ctx id n =
  LM.enter tag_test_b;
  let m = (
    wrap_tr (LM.enter tag_proj0;
             let m = project_tree ctx id 1L 32 in
             LM.exit tag_proj0;
             m) >>=? fun t0' ->
    let cms = rand_cms to_insert in
    (LM.enter tag_insert;
    let m = Sapling_storage.Commitments.add
              ctx id cms (Int64.of_int n) in
    LM.exit tag_insert;
    m)
    >>= wrap >>=? fun (ctx, _size) ->
    wrap_tr (LM.enter tag_proj1;
            let m = project_tree ctx id 1L 32 in
            LM.exit tag_proj1;
            m) >>=? fun t1' ->
    let (vs, pos, h, t0', t1') =
      (cms, (Stdint.Uint32.of_int n), 32, t0', t1') in
    let _ = (LM.enter tag_pre;
            let b = insert_list_pre vs pos h t0' in
            LM.exit tag_pre;
            b) in
    let b = (LM.enter tag_post;
            let b = insert_list_post vs pos h t0' t1' in
            LM.exit tag_post;
            b) in
    return b)
  in
  LM.exit tag_test_b;
  m


let _ =
  let n = initial_size in
  LM.start_profiling ();
  Lwt_main.run (
    make_context n >>=? fun (ctx, id) ->
    test_a ctx id n >>=? fun _ ->
    test_a_root ctx id n
    (* test_b ctx id n *)
  )
