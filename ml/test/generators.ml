open Tree
open Tezos_sapling

module Core = Core.Client
module Storage = Storage.Make_Storage (Core)

(* Arbitrary hash generator *)
let rand_hash _ =
  Core.Hash.of_bytes_exn (Bytes.create 32)
let rand_commitment _ =
  Core.Hash.to_commitment (rand_hash ())

let rand_cms n = List.init n (fun _ -> rand_commitment ())

(* Internal API *)
(* IMT generator:
  - Generate a random height h in [1,32]
  - Generate a random size s in [0, pow2 h]
  - Generate a list of commitments of length s and build the tree
    (for now, we build it using the Tree.insert_list function)
*)

let max_h = 15

let imt_gen = QCheck.Gen.(
  int_range 1 max_h >>= fun h ->
  int_bound (Stdint.Uint32.to_int (Tree.pow2_32 h)) >>= fun s ->
  let cms = List.init s (fun _ -> rand_commitment ()) in
  return (Tree.insert_list
            cms
            (Stdint.Uint32.of_int 0)
            h
            Tree.Empty)
)

let imt_h_s_gen = QCheck.Gen.(
  int_range 1 max_h >>= fun h ->
  int_bound (Stdint.Uint32.to_int (Tree.pow2_32 h)) >>= fun s ->
  let cms = List.init s (fun _ -> rand_commitment ()) in
  return ((Tree.insert_list
            cms
            (Stdint.Uint32.of_int 0)
            h
            Tree.Empty), h, Stdint.Uint32.of_int s)
)

let arbitrary_imt = QCheck.make imt_gen

(* Generators for packed function arguments. Based on the extracted templates
   form _out/Tree_PrePost.ml *)

let (gen_insert_model_args :
  (Commitments.t_C,
    (Int63.t, (Commitments.t_C, Commitments.t_H) Tree_Data.tree)
      Prims.dtuple2)
    Prims.dtuple2 FStar_QCheck.arbitrary)
  =
  QCheck.make QCheck.Gen.(
      imt_h_s_gen >>= fun (t, h, s) ->
      return (Prims.Mkdtuple2 (rand_commitment(), Prims.Mkdtuple2 (h, t))))

let (gen_insert_pow_args :
  (Commitments.t_C,
    (Stdint.Uint32.t,
      (Int63.t, (Commitments.t_C, Commitments.t_H) Tree_Data.tree)
        Prims.dtuple2)
      Prims.dtuple2)
    Prims.dtuple2 FStar_QCheck.arbitrary)
  =
  QCheck.make QCheck.Gen.(
      imt_h_s_gen >>= fun (t, h, s) ->
      return (Prims.Mkdtuple2
                (rand_commitment(), Prims.Mkdtuple2
                   (s, Prims.Mkdtuple2
                      (h, t)))))

let (gen_insert_list_args :
  (Commitments.t_C list,
    (Stdint.Uint32.t,
      (Int63.t, (Commitments.t_C, Commitments.t_H) Tree_Data.tree)
        Prims.dtuple2)
      Prims.dtuple2)
    Prims.dtuple2 FStar_QCheck.arbitrary)
  =
  QCheck.make QCheck.Gen.(
      imt_h_s_gen >>= fun (t, h, s) ->
      int_bound Stdint.Uint32.(to_int (Tree.pow2_32 h - s)) >>= fun n_to_add ->
      let cms = List.init n_to_add (fun _ -> rand_commitment ()) in
      return (Prims.Mkdtuple2
                (cms, Prims.Mkdtuple2
                   (s, Prims.Mkdtuple2
                      (h, t)))))
