open Tree
open Tree_InsertionIdentities
open Generators

(* Extracted tests, copied from _out/Tree_PrePost.ml *)

open Prims

let (insert_model_pre :
  Commitments.t_C ->
    Int63.t ->
      (Commitments.t_C, Commitments.t_H) Tree_Data.tree -> bool)
  =
  fun v ->
    fun h ->
      fun t ->
        (((Int63.gte h (0)) &&
            (Int63.lte h Commitments.max_height))
           &&
           (((Tree_Properties.incremental h t) &&
               (Tree_Properties.merkle h t))
              && (Prims.op_Negation (Tree_Properties.full t))))
          &&
          ((Tree_Properties.count_leaves t) <
             (Prims.pow2 (Prims.of_int (32))))
let (insert_model_post :
  Commitments.t_C ->
    Int63.t ->
      (Commitments.t_C, Commitments.t_H) Tree_Data.tree ->
        (Commitments.t_C, Commitments.t_H) Tree_Data.tree -> bool)
  =
  fun v ->
    fun h ->
      fun t ->
        fun t' ->
          ((Tree_Properties.incremental h t') &&
             (Tree_Properties.merkle h t'))
            &&
            (((Tree_Properties.count_leaves t') =
                ((Tree_Properties.count_leaves t) + Z.one))
               &&
               ((Tree_Methods.get h t'
                   (Util.(Stdint.Uint32.of_int << Z.to_int) (Tree_Properties.count_leaves t)))
                  = v))

let (test_insert_model_spec : FStar_QCheck.test_t) =
  FStar_QCheck.test_make "insert_model_spec" gen_insert_model_args
    (fun a0 ->
       match a0 with
       | Prims.Mkdtuple2 (v, Prims.Mkdtuple2 (h, t)) ->
           (FStar_QCheck.assume_ (insert_model_pre v h t);
            insert_model_post v h t
              (Tree_InsertionIdentities.insert_model v h t)))

let (insert_pow_pre :
              Commitments.t_C ->
                Stdint.Uint32.t ->
                  Int63.t ->
                    (Commitments.t_C, Commitments.t_H) Tree_Data.tree -> bool)
              =
              fun v ->
                fun pos ->
                  fun h ->
                    fun t ->
                      (((Int63.gte h (0)) &&
                          (Int63.lte h Commitments.max_height))
                         &&
                         (((Tree_Properties.incremental h t) &&
                             (Tree_Properties.merkle h t))
                            && (Prims.op_Negation (Tree_Properties.full t))))
                        && ((Util.(Z.of_int << Stdint.Uint32.to_int) pos) = (Tree_Properties.count_leaves t))

let (insert_pow_post :
              Commitments.t_C ->
                Stdint.Uint32.t ->
                  Int63.t ->
                    (Commitments.t_C, Commitments.t_H) Tree_Data.tree ->
                      (Commitments.t_C, Commitments.t_H) Tree_Data.tree -> bool)
              =
              fun v ->
                fun pos ->
                  fun h ->
                    fun t ->
                      fun t' ->
                        ((Tree_Properties.incremental h t') &&
                           (Tree_Properties.merkle h t'))
                          &&
                          (((Tree_Methods.to_list t') =
                              ((@) (Tree_Methods.to_list t) [v]))
                             &&
                             ((Tree_Properties.count_leaves t') =
                                ((Tree_Properties.count_leaves t) + Z.one)))
let (test_insert_pow_spec : FStar_QCheck.test_t) =
  FStar_QCheck.test_make "insert_pow_spec" gen_insert_pow_args
    (fun a0 ->
       match a0 with
       | Prims.Mkdtuple2 (v, Prims.Mkdtuple2 (pos, Prims.Mkdtuple2 (h, t)))
           ->
           (FStar_QCheck.assume_ (insert_pow_pre v pos h t);
            insert_pow_post v pos h t
              (Tree_InsertionIdentities.insert_pow v pos h t)))

let (insert_list_pre :
              Commitments.t_C list ->
                Stdint.Uint32.t ->
                  Int63.t ->
                    (Commitments.t_C, Commitments.t_H) Tree_Data.tree -> bool)
              =
              fun vs ->
                fun pos ->
                  fun h ->
                    fun t ->
                      (((Int63.gte h (0)) &&
                          (Int63.lte h Commitments.max_height))
                         &&
                         ((Tree_Properties.incremental h t) &&
                            (Tree_Properties.merkle h t)))
                        &&
                        (((Util.(Z.of_int << List.length) vs) <=
                            ((Prims.pow2 (Tree_Data.v63 h)) - (Util.(Z.of_int << Stdint.Uint32.to_int) pos)))
                           && ((Util.(Z.of_int << Stdint.Uint32.to_int) pos) = (Tree_Properties.count_leaves t)))

let (insert_list_post :
              Commitments.t_C list ->
                Stdint.Uint32.t ->
                  Int63.t ->
                    (Commitments.t_C, Commitments.t_H) Tree_Data.tree ->
                      (Commitments.t_C, Commitments.t_H) Tree_Data.tree -> bool)
              =
              fun vs ->
                fun pos ->
                  fun h ->
                    fun t ->
                      fun t' ->
                        ((Tree_Properties.incremental h t') &&
                           (Tree_Properties.merkle h t'))
                          &&
                          ((Tree_Methods.to_list t') =
                             ((@) (Tree_Methods.to_list t) vs))

let (test_insert_list_spec : FStar_QCheck.test_t) =
  FStar_QCheck.test_make "insert_list_spec" gen_insert_list_args
    (fun a0 ->
       match a0 with
       | Prims.Mkdtuple2 (vs, Prims.Mkdtuple2 (pos, Prims.Mkdtuple2 (h, t)))
           ->
           (FStar_QCheck.assume_ (insert_list_pre vs pos h t);
            insert_list_post vs pos h t (Tree_Methods.insert_list vs pos h t)))

let run_tests _ =
  QCheck_runner.run_tests [   test_insert_model_spec
                            ; test_insert_pow_spec
                            ; test_insert_list_spec
                          ]

let _ = run_tests ()
