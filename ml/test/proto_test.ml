module L = List
open Tezos_protocol_alpha
open Tezos_alpha_test_helpers
open Tezos_error_monad
open Error_monad
open Tezos_lwt_result_stdlib
open Protocol
open Lwtreslib.Bare
open Sapling_helpers.Common
open Tezos_stdlib.Utils.Infix
module R = Tezos_raw_protocol_alpha
module H = Tezos_sapling.Core.Client.Hash

open Tree
open Tree_Properties

open Tezos_sapling
module Core = Core.Client
module Storage = Storage.Make_Storage (Core)

(* Arbitrary hash generator *)
let rand_hash _ =
  Core.Hash.of_bytes_exn (Bytes.create 32)
let rand_commitment _ =
  Core.Hash.to_commitment (rand_hash ())
let rand_cms n = L.init n (fun _ -> rand_commitment ())

(* Left and right subtrees of a given node *)
let left node = Int64.mul node 2L
let right node = Int64.(add (mul node 2L) 1L)

(* Wrap trace *)
let wrap_tr m =
  m >|= function
  | Ok x -> Ok x
  | Error e -> Error (Environment.wrap_tztrace e)

(* Projection function: construct the ADT tree stored in the context at a
   given node. *)
let rec project_tree c i node h =
  R.Storage.Sapling.Commitments.find (c, i) node
  >>=? (function
  | (ctx, None) -> return Empty
  | (_, Some hash) ->
    if h = 0
    then return (Leaf (H.to_commitment hash))
    else (
      project_tree c i (left node) (h - 1)
      >>=? fun l ->
      project_tree c i (right node) (h - 1)
      >>=? fun r ->
      return (Node (hash, l, r))
    ))

(* Make a context that stores a tree with n leaves
   We make use of the [add] function, taking care of its limit of
   1000 insertions *)
let make_context n =
  let rec add_safe ctx id n pos =
    if n <= 1000
    then
      (let cms = rand_cms n in
      Sapling_storage.Commitments.add ctx
                                      id
                                      cms
                                      (Int64.of_int pos)
      >>= wrap
      >>=? fun (ctx, _size) ->
      return (ctx, id))
    else
      (let cms = rand_cms 1000 in
      Sapling_storage.Commitments.add ctx
                                      id
                                      cms
                                      (Int64.of_int pos)
      >>= wrap
      >>=? fun (ctx, _size) ->
      add_safe ctx id (n - 1000) (Int.add pos 1000)
      )
  in
    Context.init 1
    >>=? fun (b, _) ->
    Raw_context.prepare
      b.context
      ~level:b.header.shell.level
      ~predecessor_timestamp:b.header.shell.timestamp
      ~timestamp:b.header.shell.timestamp
      ~fitness:b.header.shell.fitness
    >>= wrap
    >>=? fun ctx ->
    Lazy_storage_diff.fresh
      Lazy_storage_kind.Sapling_state
      ~temporary:false
      ctx
    >>= wrap
    >>=? fun (ctx, id) ->
    Sapling_storage.init ctx id ~memo_size:0
    >>= wrap
    >>=? fun ctx ->
    add_safe ctx id n 0
    >>=? fun (ctx, id) ->
    return (ctx, id)

(* (t, id, n, to_add):
   Type for contexts containing a tree of size n at node 1 in which
   to_add leaves will be inserted *)
type treeCtx = Raw_context.t * R.Storage.Sapling.id * int * int

exception Lwt_Error

(* treeCtx generator, uses make_context *)
let (gen_treeCtx : treeCtx QCheck.arbitrary) =
  let gen = QCheck.Gen.(
    let max_n = 10000 in
    let max_to_add = 1000 in (* Actual limit of the [add] function *)
    int_bound max_n >>= fun n ->
    int_bound max_to_add >>= fun to_add ->
    match Lwt_main.run (make_context n) with
    | Error _ -> raise Lwt_Error
    | Ok (ctx, id) -> return (ctx, id, n, to_add)
  )
  in
  QCheck.make gen

(* Test A: insert' . proj = proj . insert *)
(* Thoroughly discussed on issue #23 *)
let test_a =
  QCheck.Test.make
    ~name:"Test A"
    ~count:1000
    gen_treeCtx
    (fun (ctx, id, n, to_add) ->
      let m = (
        wrap_tr (project_tree ctx id 1L 32) >>=? fun t0' ->
        let cms = rand_cms to_add in
        Sapling_storage.Commitments.add ctx id cms (Int64.of_int n)
        >>= wrap >>=? fun (ctx, _size) ->
        wrap_tr (project_tree ctx id 1L 32) >>=? fun t1' ->
        let t1'' = insert_list cms (Stdint.Uint32.of_int n) 32 t0' in
        return (t1', t1'')
      )
      in
      match Lwt_main.run m with
      | Error _ -> raise Lwt_Error
      | Ok (t1', t1'') -> t1' = t1''
    )

let (insert_list_pre :
    Commitments.t_C list ->
      Stdint.Uint32.t ->
        Int63.t ->
          (Commitments.t_C, Commitments.t_H) Tree_Data.tree -> bool)
    =
    let open Prims in
    fun vs ->
      fun pos ->
        fun h ->
          fun t ->
            (((Int63.gte h (0)) &&
                (Int63.lte h Commitments.max_height))
               &&
               ((Tree_Properties.incremental h t) &&
                  (Tree_Properties.merkle h t)))
              &&
              (((Util.(Z.of_int << List.length) vs) <=
                  ((Prims.pow2 (Tree_Data.v63 h)) - (Util.(Z.of_int << Stdint.Uint32.to_int) pos)))
                 && ((Util.(Z.of_int << Stdint.Uint32.to_int) pos) = (Tree_Properties.count_leaves t)))
let (insert_list_post :
    Commitments.t_C list ->
      Stdint.Uint32.t ->
        Int63.t ->
          (Commitments.t_C, Commitments.t_H) Tree_Data.tree ->
            (Commitments.t_C, Commitments.t_H) Tree_Data.tree -> bool)
    =
    let open Prims in
    fun vs ->
      fun pos ->
        fun h ->
          fun t ->
            fun t' ->
              ((Tree_Properties.incremental h t') &&
                 (Tree_Properties.merkle h t'))
                &&
                ((Tree_Methods.to_list t') =
                   ((@) (Tree_Methods.to_list t) vs))
(* pre ==> post *)
let test_b =
  QCheck.Test.make
    ~name:"Test B"
    ~count:1000
    gen_treeCtx
    (fun (ctx, id, n, _to_add) ->
      let m = (
        wrap_tr (project_tree ctx id 1L 32) >>=? fun t0' ->
        let cms = rand_cms 100 in
        Sapling_storage.Commitments.add ctx id cms (Int64.of_int n)
        >>= wrap >>=? fun (ctx, _size) ->
        wrap_tr (project_tree ctx id 1L 32) >>=? fun t1' ->
        return (cms, (Stdint.Uint32.of_int n), 32, t0', t1')
      )
      in
      match Lwt_main.run m with
      | Error _ -> raise Lwt_Error
      | Ok (vs, pos, h, t0', t1') ->
        QCheck.assume(insert_list_pre vs pos h t0');
        insert_list_post vs pos h t0' t1'
    )

let run_tests _ =
  QCheck_runner.run_tests [ test_a;
                            test_b
                          ]

let _ = run_tests ()
