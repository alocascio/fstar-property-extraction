module Tree.Properties

module C = Commitments
module UInt32 = FStar.UInt32
module Int64 = FStar.Int64

open Tree.Data

// valid checks wether we put Empty constructors in the right place:
// either if the tree is really empty, or to represent a _cut_ of a
// sub-tree full of default values. Thus, we shoud never have a `Node
// Empty Empty` tree, as this would allow to construct sub-trees with
// gaps.
val valid : tree 'a 'b -> bool
let rec valid = function
  | Empty -> true
  | Leaf _ -> true
  | Node _ Empty Empty -> false
  | Node _ l r -> valid l && valid r

// A node is full if all paths from the root finish in a Leaf. Thus,
// there is no Empty subnode of a full tree.

val full : tree 'a 'b -> Tot bool
let rec full  = function
  | Empty -> false
  | Leaf _ -> true
  | Node _ l r -> (full l) && (full r)

val full_is_valid : t:tree 'a 'b ->
    Lemma (requires (full t))
          (ensures (valid t))
          [SMTPat (full t)]
let rec full_is_valid  = function
  | Leaf _ -> ()
  | Node _ l r -> full_is_valid l; full_is_valid r

// Incremental Merkle Trees are filled _incrementally_ from left to
// right, without gaps. That is, without any [Empty] subtree in
// between leaves at the same height:

val left_leaning : t:tree 'a 'b -> Tot bool
let rec left_leaning  = function
  | Empty -> true
  | Leaf _ -> true
  | Node _ l Empty -> left_leaning l
  | Node _ l r -> (full l) && (left_leaning r)

val full_leans_left : t:tree 'a 'b ->
    Lemma (requires True) (ensures (full t ==> left_leaning t))
    [SMTPat (full t)]
let rec full_leans_left = function
  | Empty | Leaf _ -> ()
  | Node _ l r -> full_leans_left l ; full_leans_left r

// TODO this is only used for the spec of insert and forces the type
// to be eqtype, can we remove it?
val mem : #a:eqtype -> #b:Type -> a -> tree a b -> Tot bool
let rec mem #a #_ cm = function
  | Empty -> false
  | Leaf v -> cm = v
  | Node _ l r -> mem cm l || mem cm r

// t has at least one leaf at depth `height t`
// works also on unbalanced
val height : (t:tree 'a 'b{valid t /\ not (Empty? t)}) -> Tot nat
let rec height = function
  | Leaf _ -> 0
  | Node _ l r ->
      match l,r with
      | Empty, _ -> height r + 1
      | _, Empty -> height l + 1
      | _ ->
        let hl = height l in
        let hr = height r in
        if hl > hr then (hl+1) else (hr+1) // couldn't find max;

// The has_height predicate binds the fact that the h argument in the
// specification should prescribe the height of the IMT. Since we
// overload the Empty constructor to also denote the optimization of a
// subtree full of pre-defined values, we will allow Empty trees to
// "declare" any height. Thus, has_height is indeed an upper bound on
// the height of the tree.

val has_height : h:C.v_height -> t:tree 'a 'b{valid t} -> bool
let has_height h  = function
    | Empty -> true
    | t -> height t = Int63.v h

// IMTs model complete, balanced Merkle trees. However, since they do
// so by overloading Empty to denote _cuts_, we need a tailored
// version of the traditional definition of a _balanced binary tree_
// which accounts for the left-leaning skew.

val balanced : t:tree 'a 'b{valid t} -> bool
let rec balanced = function
  | Empty -> true
  | Leaf _ -> true
  | Node _ l r ->
    match l,r with
    | Empty, _ -> balanced r
    | _, Empty -> balanced l
    | _ ->
        let hl = height l in
        let hr = height r in
        hl = hr && balanced l && balanced r

// We pack the four properties that make a tree a proper Incremental
// Merkle tree of height h. This will serve as the precondition of all
// functions in internal/lower-level API below.

val incremental : h:C.v_height -> t:tree 'a 'b -> bool
let incremental h t =
    valid t && left_leaning t && balanced t && has_height h t

// number of leaves of a tree
val count_leaves : tree 'a 'b -> Tot nat
let rec count_leaves = function
  | Empty -> 0
  | Leaf _ -> 1
  | Node _ l r -> (count_leaves l) + (count_leaves r)


val max_leaves : h:C.v_height -> t:tree 'a 'b ->
  Lemma (requires (valid t /\ has_height h t /\ balanced t))
        (ensures (count_leaves t <= pow2 (v63 h)))
	(decreases %[v63 h])
let rec max_leaves h t =
    match t with
    | Empty | Leaf _ -> ()
    | Node _ l r ->
        max_leaves (pred63 h) l;
        max_leaves (pred63 h) r

val max_leaves_full : h:C.v_height -> t:tree 'a 'b ->
  Lemma (requires (valid t /\ has_height h t /\ balanced t))
        (ensures (count_leaves t = pow2 (v63 h) <==> full t))
	(decreases %[v63 h])
let rec max_leaves_full  h t =
    match t with
    | Empty | Leaf _ -> ()
    | Node _ l r ->
      let hs = pred63 h in
      max_leaves hs l;
      max_leaves hs r;
      max_leaves_full hs l;
      max_leaves_full hs r

val lemma_tree_leaves_bound :
  h:C.v_height ->
  t:tree 'a 'b{incremental h t} ->
  Lemma (requires true)
  	(ensures (count_leaves t <= pow2 (v63 h)))
  	(decreases %[v63 h])
let rec lemma_tree_leaves_bound h t =
  match t with
  | Empty -> ()
  | Leaf _ -> ()
  | Node _ l r ->
    lemma_tree_leaves_bound (pred63 h) l;
    lemma_tree_leaves_bound (pred63 h) r

val lemma_incremental_full_left :
  h:C.v_height ->
  t:tree 'a 'b{incremental h t} ->
  Lemma (requires (Node? t /\ count_leaves t >= pow2 ((v63 h) - 1)))
        (ensures (count_leaves (Node?.l t) = pow2 ((v63 h) - 1)))
let lemma_incremental_full_left h t =
  let (Node _ l r) = t in
  if full l then
    max_leaves_full (pred63 h) l
  else
    lemma_tree_leaves_bound (pred63 h) l

val lemma_incremental_empty_right :
  h:C.v_height ->
  t:tree 'a 'b{incremental h t} ->
  Lemma (requires (Node? t /\ count_leaves t < (pow2 ((v63 h) - 1))))
        (ensures (count_leaves (Node?.l t) = count_leaves t /\
                  Empty? (Node?.r t)))
let lemma_incremental_empty_right h t =
  let (Node _ l r) = t in
  if full l then (
    max_leaves_full (pred63 h) l;
    assert(count_leaves l = pow2 (v63 (pred63 h)));
    assert(count_leaves t > count_leaves l)
    )
  else
    lemma_tree_leaves_bound (pred63 h) l

val lemma_nth_index : #a:eqtype -> l:list a ->
    Lemma (ensures forall (n:nat). (n < List.length l ==>
                      (List.Tot.nth l n = Some (List.Tot.index l n)))
                /\ (n >= List.length l ==> List.Tot.nth l n = None))
let rec lemma_nth_index #a l = match l with
  | [] -> ()
  | v::vs -> lemma_nth_index vs

open FStar.List.Tot

val lemma_index_prefix : #a:eqtype -> l1:list a -> l2:list a -> i:nat ->
    Lemma (requires (i < List.length l1))
          (ensures (List.Tot.index l1 i = List.Tot.index (l1@l2) i))
let rec lemma_index_prefix #a l1 l2 i =
match l1 with
| [] -> ()
| e::es ->
    if i = 0 then () else
    lemma_index_prefix es l2 (i-1)

val lemma_index_postfix : #a:eqtype -> l1:list a -> l2:list a -> i:nat ->
    Lemma (ensures (i >= List.length l1 /\ i < List.length (l1@l2)
                   ==> (List.Tot.index l2 (i- List.length l1) =
                      List.Tot.index (l1@l2) i)))
let rec lemma_index_postfix #a l1 l2 i =
match l1 with
| [] -> ()
| e::es ->
    if i = 0 then () else
    lemma_index_postfix es l2 (i-1)

///////

// Gets the root hash at a certain [height]. If the tree is [Empty],
// we use the [uncomitted] list to get the corresponding hash.
val get_root_height : t:tree C.t_C C.t_H -> h:C.v_height ->
  Pure C.t_H (requires (valid t /\ has_height h t))
             (fun _ -> True)
let get_root_height tree height =
  //assert (Compare.Int.(height >= 0 && height <= 32)) ;
  match tree with
  | Empty ->
      C.uncommitted height
  | Node h _ _ ->
      h
  | Leaf h ->
      C.hash_of_commitment h

// Apply the [merkle_hash] function over the root hashes of
// the subtrees [t1] and [t2].
val hash : h:C.v_height ->
  t1:tree C.t_C C.t_H{(valid t1 /\ has_height h t1)} ->
  t2:tree C.t_C C.t_H{(valid t2 /\ has_height h t2)} -> C.t_H
let hash height t1 t2 =
  C.merkle_hash
    height
    (get_root_height t1 height)
    (get_root_height t2 height)

// Merkle tree property.
val merkle : h:C.v_height ->
  t:tree C.t_C C.t_H{valid t /\ has_height h t /\ balanced t} ->
  Tot bool
  (decreases %[v63 h])
let rec merkle h t =
  match t with
  | Empty -> true
  | Leaf _ -> true
  | Node ha l r ->
    let hs = pred63 h in
    ha = hash hs l r && merkle hs l && merkle hs r

// Type synonym for "compressed" IMTs of height h.
inline_for_extraction
type imt (h:C.v_height) : Type  =
   t:tree C.t_C C.t_H {incremental h t && merkle h t}

///////////////////////////
// High level interface
///////////////////////////

val hvalid : htree 'a 'b -> bool
let hvalid (on, t) =
  count_leaves t = Int64.v (size (on, t)) &&
  incremental default_height t

val hmerkle : htree C.t_C C.t_H -> bool
let hmerkle (n, t) =
  valid t &&
  balanced t &&
  has_height default_height t &&
  merkle  default_height t

val hfull : htree 'a 'b -> bool
let hfull (n, t) =
    full t

inline_for_extraction
type himt =  ht:htree C.t_C C.t_H{hvalid ht /\ hmerkle ht}

val v63_lit_inv : h:(FStar.Int.int_t 63){h >= 0 /\ h <= Int63.v C.max_height} ->
  Lemma (v63 (lit h) = h)
let v63_lit_inv _ = ()

// AL: Don't know why this is needed. Without it the assertion fails...
val lem_default : unit -> Lemma (v63 default_height = 32)
let lem_default _ = ()

val lem_hfull_none : ht:himt ->
  Lemma (hfull ht <==> None? (fst ht))
  [SMTPat (hfull ht)]
#push-options "--z3rlimit 20"
let lem_hfull_none (on, t) =
  max_leaves_full default_height t;
  assert(None? on ==> full t);
  assert(full t ==> count_leaves t = pow2 (v63 default_height));
  assert(Some? on ==> count_leaves t = UInt32.v (Some?.v on));
  assert(not (FStar.UInt.fits (pow2 32) 32));
  lem_default ();
  assert(v63 default_height = 32);
  assert(full t ==> pow2 32 = count_leaves t);
  assert(full t ==> not (FStar.UInt.fits (count_leaves t) 32));
  assert(full t ==> None? on)
#pop-options


let lem_max_size _ : Lemma (Int64.v max_size = pow2 32) = ()

val lem_length32_correct :
  l:list 'a ->
  Lemma (requires (length l < pow2 32))
        (ensures (UInt32.v (length32 l) = length l))
	[SMTPat (length32 l)]
let lem_length32_correct l =
  lem_max_size ();
  FStar.Math.Lemmas.small_mod (length l) (pow2 32)

val lem_add_mod : x:uint32 -> y:uint32 ->
  Lemma (UInt32.(v (add_mod x y)) =
	  (UInt32.v x + UInt32.v y) % Int64.v max_size)
let lem_add_mod x y = ()

val lem_add_length : x:uint32 -> l:list 'a ->
  Lemma (UInt32.(v (add_mod x (length32 l))) =
	  (UInt32.v x + length l) % Int64.v max_size)
#push-options "--z3rlimit 10"
let lem_add_length x l =
  lem_add_mod x (length32 l);
  assert(UInt32.(v (add_mod x (length32 l))) =
	  (UInt32.v x + UInt32.v (length32 l)) % Int64.v max_size);
  assert(UInt32.v (length32 l) = length l % Int64.v max_size);
  assert(UInt32.(v (add_mod x (length32 l))) =
	  (UInt32.v x + (length l % Int64.v max_size)) % Int64.v max_size);
  FStar.Math.Lemmas.lemma_mod_plus_distr_r
   (UInt32.v x) (length l) (Int64.v max_size);
  assert(((UInt32.v x + (length l % Int64.v max_size)) % Int64.v max_size) =
         (UInt32.v x + length l) % Int64.v max_size)
#pop-options

val lem_mod_0 : a:nat -> b:nat ->
  Lemma (requires (a > 0 /\ a <= b /\ a % b = 0))
        (ensures (a = b))
let lem_mod_0 a b = ()

val lem_mod_pos : a:nat -> b:nat ->
  Lemma (requires (a > 0 /\ a <= b /\ a % b > 0))
        (ensures (a < b))
let lem_mod_pos a b = ()

val list_left_leaning : #a:eqtype -> l:list a ->
  Lemma (ensures (forall (i:nat) (j:nat).
                 (None? (List.Tot.nth l i) /\ i<j) ==> None? (List.Tot.nth l j)))
let rec list_left_leaning #a = function
    | [] -> ()
    | v::vs -> list_left_leaning vs
